#!/usr/bin/env bb
(ns mj
  (:require [babashka.curl :as curl]
            [cheshire.core :as json]))

(def arn "3r0kkivk1c")
(def base-url (str "https://" arn ".execute-api.eu-west-3.amazonaws.com/tasks"))

(defn get-tasks
  []
  (let [response (curl/get base-url)]
    (json/parse-string (:body response))))

(defn due?
  [task]
  (not (pos? (get task "daysToTarget"))))

(defn print-list
  [tasks]
  (doseq [task tasks]
    (let [id (get task "id")
          label (get task "name")]
    (printf "[%2s] %s%n" id label))))

(defn execute-task
  [id]
  (let [response (curl/put (str base-url "/" id "/execution")
                           {:query-params {"jarvis" "please"}})]
    (printf "[%2s] %s" id (:body response))))

(defn -main
  [args]
  (if (seq args)
    (let [task-id (Integer/parseInt (first args))]
      (execute-task task-id))
    (->> (get-tasks)
         (filter due?)
         (print-list))))

(when (= *file* (System/getProperty "babashka.file"))
  (-main *command-line-args*))

(comment
  (-main []))
