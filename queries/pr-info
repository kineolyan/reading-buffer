fragment prNodes on PullRequest {
  # Get the number of comments from review threads
  # It could be interesting to see how many threads there are as well as the number of comments per thread
  # ?Filter to remove author's comments?
  reviewThreads(first: 10) {
    nodes {
      comments {
        totalCount
      }
    }
  }
}

# Obtain the values from an inner sequence
query MyQuery {
  node(id: "PR_kwDODnLics45gUMs") { # id of the wanted value
    ... on PullRequest {
      id
      commits(last: 2, before: "MTc") { # cursors and offsets
        nodes {
          commit {
            authoredDate
            message
          }
        }
      }
    }
  }
}

# Inspect the rate limit and remaining capabilities
# https://docs.github.com/en/graphql/overview/resource-limitations#returning-a-calls-rate-limit-status
# Could be attached to any query, decrementing `(remaining, resetAt)` with every call
query {
  viewer {
    login
  }
  rateLimit {
    limit
    cost
    remaining
    resetAt
  }
}
