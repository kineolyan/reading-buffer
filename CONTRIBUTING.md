# Contributing to Jarvis

## Context

This project is a personal project, used every day by myself and my family.<br/>
That being said, it is public in the open for anyone interested to use it.

Contributions to the projects are open for the Hacktoberfest, to welcome anyone willing to spend some time on another small project, for fun, to do some PRs/MRs without fear of being judged and without diving in a very active project already targeted by many others.

## How can I contribute?

Merge Requests are welcome for issues labeled with [`hacktoberfest`](https://gitlab.com/kineolyan/reading-buffer/-/issues?label_name=hacktoberfest) are a good start.<br>
If you find something else "valuable" to offer (additional tests, suggestions to improve the codebase), feel free to submit a MR.

### Process

 1. Comment on an issue to get assigned to it.
 1. Fork the project
 1. Make your changes
 1. Open a Merge Request for integration

Thank you in advance for any work in this project :)
