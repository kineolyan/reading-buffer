# Collections of personal online applications

## Features

### online-reading-buffer

Application storing links to read, stored as:
 - _standard_, for common articles or videos,
 - _series_ for links with many sections or pages to read
 - _stash_ for a temporary buffer of links, useful for sharing them between two devices

### Peter

Task application recording actions to repeat on a regular basis or one-time tasks.

The main feature of this is that the more tasks are "forgotten", the higher and redder they appear in the list. This prevents me from forgetting too much to do them.<br>
When a task is complete, its next execution is computed. This is more accurate than setting a cron for a task, not taking the delay to execute them into account.

## Developing

All apps are developed in a single Clojurescript environment, managed by Shadow-cljs.

Babashka tasks provide helper tasks to assist in the development.

This project does not contain any particular tests. It is currently manually tested.

## Troubleshoot

### JVM crashs

It is possible to see JVM crashes when using some javascript libraries (possibly coming with clojurescript libraries).

See the following discussing the issue:
 - https://github.com/thheller/shadow-cljs/issues/1015
 - https://github.com/google/closure-compiler/issues/3945

In this case, a working solution is to set `JAVA_TOOL_OPTIONS` to a larger heap or bigger stack size:
 - heap: `-Xms1g -Xmx3g`
 - stack size: `-Xss8m`
