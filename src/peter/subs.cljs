(ns peter.subs
  (:require
   [re-frame.core :as rf]
   [peter.task :as t]))

(rf/reg-sub
 ::status
 (fn [db]
   (:connected? db)))

(rf/reg-sub
 ::api
 (fn [db]
   {:token (:token db)}))

(defn compute-additional-attributes
  [entry]
  (assoc entry
         :days-to-target (t/compute-days-to-target entry)))

(comment
  (->>
   (deref re-frame.db/app-db)
   :entries
   (map compute-additional-attributes))
  (deref (rf/subscribe [::tasks])))

(rf/reg-sub
 ::tasks
 (fn [{:keys [entries entry-order]}]
   (->> entries
        (filter #(not (get-in % [:frequency :done])))
        (map compute-additional-attributes)
        (sort-by
         #(get entry-order (:id %) 0)))))

(defn find-last-due-task-id
  "Finds the id of the last task due in `n` days."
  [entries n]
  (->> (filter #(<= (:days-to-target %) n) entries)
       last
       :id))

(defn find-first-next-task-id
  "Finds the id of the first task due in more than `n` days."
  [entries n]
  (when-let [last-id (find-last-due-task-id entries n)]
    (->> (drop-while #(not= (:id %) last-id) entries)
         fnext
         :id)))

(comment
  ; future tasks
  (find-last-due-task-id @(rf/subscribe [::tasks]) 1)
  (find-first-next-task-id @(rf/subscribe [::tasks]) 1)
  ; edge case where all tasks are done, simulated with a negative number of days
  (find-last-due-task-id @(rf/subscribe [::tasks]) -37)
  (find-first-next-task-id @(rf/subscribe [::tasks]) -37))

(rf/reg-sub
 ::next-tasks
 :<- [::tasks]
 (fn [entries [_ due]]
   (if-let [id (find-first-next-task-id entries due)]
     (take-while #(not= (:id %) id) entries)
     [])))

(rf/reg-sub
 ::other-tasks
 :<- [::tasks]
 (fn [entries [_ due]]
   (if-let [id (find-first-next-task-id entries due)]
     (drop-while #(not= (:id %) id) entries)
     entries)))

(comment
  ; testing task list when all is done
  (rf/subscribe [::next-tasks -37])
  (rf/subscribe [::other-tasks -37]))

(rf/reg-sub
 ::show-more
 (fn [db]
   (:show-more db)))

(rf/reg-sub
 ::editing-token?
 (fn [db]
   (boolean (:editing-token? db))))

(rf/reg-sub
 ::displayed-editor
 (fn [db]
   (:editor db)))
