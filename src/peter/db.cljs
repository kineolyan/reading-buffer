(ns peter.db)

(def default-db
  {:editor nil
   :show-more false})

(def storage-key "peter-token")
