(ns peter.events
  (:require [clojure.string :as str]
            [re-frame.core :as rf]
            [day8.re-frame.tracing :refer-macros [fn-traced]]
            [day8.re-frame.http-fx]
            [com.rpl.specter :as s :refer-macros [transform]]
            [shared.fauna-fx :as f]
            [shared.fauna-query :as fq]
            [shared.fauna-date :as fd]
            [peter.db :as db]
            [peter.task :as t]))

; (comment
;   ; how to get the type of the date objects
;   (= faunadb/values.FaunaDate
;      (-> (f/read-page f/page*)
;          :data
;          first
;          f/read-item
;          (get "due_date")
;          type)))

(defn save-query-result
  [db result]
  (-> db
      (assoc :query-result result)
      (dissoc :query-errors)))

(defn save-query-error
  [db result]
  (-> db
      (assoc :query-errors result)
      (dissoc :query-result)))

(defn get-task
  [db task-id]
  {:post [(some? %)]}
  (->> (:entries db)
       (filter #(= (:id %) task-id))
       (first)))

(defn get-task-category
  [{:keys [frequency]}]
  (cond
    (:duration frequency) :repeated
    (contains? frequency :done) :punctual))

(comment
  (map (juxt :name get-task-category)
       (-> re-frame.db/app-db deref :entries)))

(defn filterer-by-id
  [id]
  (s/filterer #(= (:id %) id)))

;;; task management

(defn update-task
  [db task-id f]
  (transform
   [:entries (filterer-by-id task-id) s/ALL]
   f
   db))

(defn flag-task
  [db task-id state]
  (update-task db task-id (if state
                            #(assoc % :peter/state state)
                            #(dissoc % :peter/state))))

(rf/reg-event-fx
 :fetch-tasks/query
 (fn-traced query-tasks
            [cofx _]
            (f/fauna-query cofx {:read {:collection "tasks"
                                      :page-size 50}
                               :on-success [:fetch-tasks/success]
                               :on-failure [:fetch-tasks/error]})))

(defn define-entry-order
  [entries]
  (reduce
   (fn [m [i entry]] (assoc m (:id entry) i))
   {}
   (map vector (range) (sort-by t/compute-days-to-target entries))))

(def str-units
  {"DAY" :day
   "WEEK" :week
   "MONTH" :month
   "YEAR" :year})

(defn parse-frequency
  "Parse the concatenation of frequency representation into a simple datum."
  [freq]
  (cond
    (:repeated freq) (update (:repeated freq) :unit str-units)
    (contains? freq :punctual) {:done (= "DONE" (:punctual freq))}
    :else {:done (get freq :punctual false)}))

(defn format-remote-entry
  [{:keys [_id name due_date frequency]}]
  (let [due-date (-> due_date .-value fd/date->timestamp)]
    {:id _id
     :name name
     :due-date due-date
     :frequency (parse-frequency frequency)}))

(defn set-tasks
  [db values]
  (let [entries (map format-remote-entry values)]
    (assoc db
           :entries entries
           :entry-order (define-entry-order entries)
           :connected? :yes)))

(defn add-task
  [db value]
  (let [new-task (format-remote-entry value)]
    (s/setval [:entries s/END] [new-task] db)))

(comment
  (set-tasks {} (-> re-frame.db/app-db deref :query-result))
  (parse-frequency {:repeated nil :punctual true})
  (parse-frequency {:repeated nil :punctual false})
  (parse-frequency {:repeated {:duration 3, :unit "MONTH"}, :punctual nil})

  (add-task
   {:entries [1]}
   {:data {:createTask (into {} [[:_id "326658037593932364"]
                                 [:name "MAJ Peter"]
                                 [:due_date "2022-06-23"]
                                 [:frequency {:repeated {:duration 3, :unit "MONTH"}, :punctual nil}]])}}))

(rf/reg-event-db
 :fetch-tasks/success
 (fn-traced process-tasks
            [db [_ result]]
            (when result
              (-> db
                  (save-query-result result)
                  (set-tasks (:items result))))))

(comment
  (rf/dispatch [:fetch-tasks/query]))

(rf/reg-event-db
 :fetch-tasks/error
 (fn-traced fail-on-tasks
            [db [_ result]]
            (-> db
                (save-query-error result)
                (assoc  :connected? :no))))

(rf/reg-event-fx
 :refresh-tasks/query
 (fn-traced
  [cofx _]
  {:db (-> (:db cofx)
           (dissoc :entries)
           (assoc :connected? :in-progress))
   :fx [[:dispatch [:fetch-tasks/query]]]}))

(def time-increments
  (as-> {:day (* 24 3600 1000)} m
    (assoc m :week (* 7 (:day m)))
    (assoc m :month (* 30 (:day m)))
    (assoc m :year (* 365 (:day m)))))

(defn compute-next-due-date
  [task]
  {:pre [(= :repeated (get-task-category task))]}
  (let [{:keys [duration unit]} (:frequency task)
        current-time (js/Date.now)]
    (+ current-time (* duration (get time-increments unit)))))

(comment
  (compute-next-due-date {:due-date 1648152720000,
                          :frequency {:duration 1, :unit :week}}))

(defn build-exec-payload
  [db task-id]
  (let [task (get-task db task-id)
        payload (case (get-task-category task)
                  :repeated {:due_date (-> (compute-next-due-date task)
                                           (fd/->fdb-date)
                                           (fq/Date.))}
                  :punctual {:frequency {:punctual "DONE"}})]
    {:update {:target {:collection "tasks"
                       :id task-id}
              :values payload}}))

(rf/reg-event-fx
 :execute-task/query
 (fn-traced
  [cofx [_ task-id]]
  (merge
   {:db (flag-task (:db cofx) task-id :updating)}
   (f/fauna-query cofx (merge (build-exec-payload (:db cofx) task-id)
                            {:on-success [:execute-task/success task-id]
                             :on-failure [:execute-task/error task-id]})))))

(comment
  (rf/dispatch [:execute-task/query 1])
  (rf/dispatch [:execute-task/success 0])
  (rf/dispatch [:execute-task/error 0])
  (rf/dispatch [:reset-task-state 0]))

(defn refresh-due-date
  [task {due-date :due_date}]
  (if due-date
    (assoc task
           :due-date (fd/date->timestamp due-date)
           :days-to-target 7)
    task))

(defn refresh-frequency
  [task {:keys [frequency]}]
  (if frequency
    (assoc task :frequency (parse-frequency frequency))
    task))

(defn refresh-task
  [db task-id result]
  (def result* result)
  (update-task
   db
   task-id
   (fn [task] (-> task
                  (refresh-frequency result)
                  (refresh-due-date result)))))

(comment
  (refresh-task
   (deref re-frame.db/app-db)
   "326658037593932364"
   {:data {:partialUpdateTask {:frequency {:punctual false}
                               :due_date "2020-10-14T12:34:56Z"}}})
  (refresh-task
   (deref re-frame.db/app-db)
   "326658037593932364"
   result*)
  (fd/->fdb-date 1648152829000)
  (fd/date->timestamp (:due_date result*))
  (refresh-due-date {} result*))

(rf/reg-event-fx
 :execute-task/success
 (fn-traced
  [cofx [_ task-id result]]
  {:db (-> (:db cofx)
           (flag-task task-id :updated)
           (refresh-task task-id result))
   :fx [[:dispatch-later {:ms 10000
                          :dispatch [:reset-task-state task-id]}]]}))

(rf/reg-event-db
 :execute-task/error
 (fn-traced
  [db [_ task-id]]
  (flag-task db task-id :error)))

(defn build-create-payload
  "Builds the graphql payload to create a new task."
  [definition]
  {:create {:collection "tasks"
            :datum (if (:target-date definition)
                     {:name (:name definition)
                      :due_date (fq/Date. (:target-date definition))
                      :frequency {:punctual "TODO"}}
                     {:name (:name definition)
                      :due_date (fq/Date. (fd/->fdb-date (js/Date.now)))
                      :frequency {:repeated {:duration (:duration definition)
                                             :unit (:unit definition)}}})}})

(comment
  (build-create-payload {:name "task-single" :target-date "2022-10-20"})
  (build-create-payload {:name "task-repeat" :duration 20 :unit "DAYS"}))

(rf/reg-event-fx
 :create-repeated-task/query
 (fn-traced
  [cofx [_ definition]]
  (f/fauna-query cofx (merge (build-create-payload definition)
                           {:on-success [:create-repeated-task/success]
                            :on-failure [:create-repeated-task/error]}))))

(rf/reg-event-db
 :create-repeated-task/success
 (fn-traced
  [db [_ result]]
  (as-> db db
    (save-query-result db result)
    (add-task db result)
    (assoc db :entry-order (define-entry-order (:entries db))))))

(rf/reg-event-db
 :create-repeated-task/error
 (fn-traced
  [db [_ result]]
  (save-query-error db result)))

(rf/reg-event-db
 :reset-task-state
 (fn-traced
  [db [_ task-id]]
  (flag-task db task-id nil)))

;;; Token management

(rf/reg-event-fx
 :start-editing-token
 (fn-traced
  [db _]
  (assoc db :editing-token? true)))

(rf/reg-event-fx
 :update-token
 (fn-traced
  [cofx [_ new-token]]
  (f/token->local-store db/storage-key new-token)
  {:db (-> (:db cofx)
           (assoc :token new-token
                  :connected? :in-progress)
           (dissoc :editing-token?))
   :fx [[:dispatch [:fetch-tasks/query]]]}))

(comment
  (rf/dispatch [:fetch-tasks/query]))

;;; UI controls

(rf/reg-event-db
 :toggle-show-more
 (fn-traced
  [db _]
  (update db :show-more not)))

(rf/reg-event-db
 :reorder-entries
 (fn-traced
  [db _]
  (assoc db :entry-order (define-entry-order (:entries db)))))

(rf/reg-event-db
 :display-editor
 (fn-traced
  [db [_ editor-id]]
  (assoc db :editor editor-id)))

(rf/reg-event-db
 :hide-editor
 (fn-traced
  [db _]
  (assoc db :editor nil)))

;;; Initialization

(rf/reg-event-fx
 ::initialize-db
 [(rf/inject-cofx :local-store-token db/storage-key)
  (rf/inject-cofx :url-token)]
 (fn-traced
  [{stored-token :local-store-token url-token :url-token} _]
  (let [token (or url-token stored-token)
        existing-token? (not (str/blank? token))]
    (when (and url-token (not= url-token stored-token))
      (f/token->local-store db/storage-key url-token))
    {:db (merge db/default-db
                {:token token}
                {:connected? (if existing-token? :in-progress :no)})
     :fx [(when existing-token? [:dispatch [:fetch-tasks/query]])]})))
