(ns peter.views
  (:require [clojure.string :as str]
            [reagent.core :as r]
            [fork.re-frame :as fork]
            [re-frame.core :as rf]
            [peter.events :as evts]
            [peter.subs :as subs]))

(defn str-val
  [value default]
  (if (str/blank? value) default value))

(defn token-input-ui
  [value]
  (let [input-val (r/atom (str-val value ""))]
    (fn [_]
      [:input {:placeholder "Name of the endpoint"
               :value @input-val
               :on-focus #(rf/dispatch [:start-editing-token])
               :on-blur #(rf/dispatch [:update-token @input-val])
               :on-change #(reset! input-val (-> % .-target .-value))}])))

(defn connection-status-ui
  []
  (let [api (rf/subscribe [::subs/api])
        status (rf/subscribe [::subs/status])
        editing? (rf/subscribe [::subs/editing-token?])]
    [:p
     "Connected to "
     [token-input-ui (:token @api)]
     " "
     [:i
      (if @editing?
        [:span "(?)"]
        (case @status
          :yes [:span "(up ✅)"]
          :no [:span "(down ⭕)"]
          :in-progress [:span "(...)"]))]]))

(defn label-to-days
  [days]
  (cond
    (pos? days) (str days " jours restant")
    (zero? days) "C'est pour aujourd'hui"
    (neg? days) (str "Dépassé de " (- days) " jours")))

(defn label-due
  [timestamp]
  (-> timestamp js/Date. .toLocaleDateString))

(def human-units
  {:day "jours"
   :week "semaines"
   :month "mois"
   :year "ans"})

(defn label-frequency
  [f]
  (when-let [{:keys [duration unit]} f]
    (str "Tous les " duration " " (get human-units unit unit))))

(comment
  (label-frequency "1d"))

(defn task-details-ui
  [{:keys [frequency due-date days-to-target]}]
  (if (:duration frequency)
    [:div
     [:span (label-frequency frequency)]
     ", "
     [:span (label-to-days days-to-target)]]
    [:div
     [:span (label-due due-date)]
     ": "
     [:span (label-to-days days-to-target)]]))

(defn execute-task-ui
  [task]
  [:div
   [:button
    (merge
     {:on-click #(rf/dispatch [:execute-task/query (:id task)])}
     (when (= (:peter/state task) :updating)
       {:disabled "disabled"}))
    (:name task)]
   (case (:peter/state task)
     :updated [:span.status "(success 👍)"]
     :error [:span.status "(oops ❌)"]
     :updating [:span.status "(...)"]
     nil)])

(defn compute-task-class
  [{n :days-to-target}]
  (cond
    (neg? n) "past-task"
    (zero? n) "day-task"
    (= 1 n) "next-task"
    (< 1 n) "future-task"))

(defn task-entry-ui
  [task]
  [:li {:key (:id task)
        :class ["task" (compute-task-class task)]}
   [:div
    [execute-task-ui task]
    [task-details-ui task]]])

(defn values->task
  [{:strs [name target-date unit duration]}]
  (cond->
   {}
    (not (str/blank? name))
    (assoc :name name)
    (not (str/blank? target-date))
    (assoc :target-date target-date)
    (not (str/blank? unit))
    (assoc :unit unit)
    (not (str/blank? duration))
    (assoc :duration (js/parseInt duration))))

(defn validate-repeated-task
  [{:keys [duration unit]}]
  (let [possible-units (set (keys evts/str-units))]
    (cond->
     {}
      (not (pos? duration))
      (assoc "duration" "La durée doit être positive")
      (not (contains? possible-units unit))
      (assoc "unit"
             (str "L'unité doit être parmi "
                  (str/join ", " (keys evts/str-units)))))))

(defn validate-single-task
  [{:keys [target-date]}]
  (cond->
   {}
    (and (not (str/blank? target-date))
         (not (re-matches #"\d{4}-\d{2}-\d{2}" target-date)))
    (assoc "target-date" "La date doit être au format yyyy-mm-dd.")))

(defn validate-task-definition [values]
  (let [{:keys [name unit target-date] :as task} (values->task values)
        has-unit? (and unit (not (str/blank? unit)))
        has-date? (and target-date (not (str/blank? target-date)))
        base-messages (cond-> {}
                        (str/blank? name)
                        (assoc "name" "Nom obligatoire"))]
    (cond
      (and has-unit? has-date?)
      (assoc base-messages "target-date"  "Date incompatible avec une répétition")
      has-unit?
      (validate-repeated-task task)
      has-date?
      (validate-single-task task)
      :else
      base-messages)))

(comment
  (values->task *1)
  (validate-task-definition {"name" ""
                             "duration" -1
                             "unit" "w"})
  (validate-task-definition {"name" "do something"
                             "duration" 3
                             "unit" "WEEK"})
  (validate-task-definition {"name" "hello"
                             "target-date" "abc"})
  (validate-task-definition {"name" "something"
                             "target-date" "2320-08-14"})
  (validate-task-definition {"name" "something"
                             "duration" "22"
                             "unit" "MONTH"
                             "target-date" "2022-10-12"}))

(defn create-task!
  [values]
  (let [task (values->task values)]
    (rf/dispatch [:create-repeated-task/query task])
    (rf/dispatch [:hide-editor])))

(defn repeated-task-form
  []
  [fork/form
   {:validation validate-task-definition
    :on-submit (fn [x] (create-task! (:values x))),
    :prevent-default? true}
   (fn [{:keys
         [values
          touched
          errors
          handle-change
          handle-blur
          handle-submit
          attempted-submissions]}]
     (let [render-error (fn [x]
                          (when (and (touched x) (get errors x))
                            (->> [(get errors x)]
                                 (map #(vector :div %))
                                 (concat [:<>])
                                 vec)))]
       [:form
        {:on-submit handle-submit}
        [:div
         [:input
          {:type "text",
           :name "name",
           :placeholder "Intitulé"
           :value (values "name"),
           :on-change handle-change,
           :on-blur handle-blur}]
         [render-error "name"]]
        [:div
         [:span "pour le "]
         [:input
          {:type "string",
           :name "target-date",
           :placeholder "yyyy-mm-dd"
           :value (values "target-date"),
           :on-change handle-change,
           :on-blur handle-blur}]
         [render-error "target-date"]]
        [:div
         [:span "tous les "]
         [:input
          {:type "number",
           :name "duration",
           :placeholder "number"
           :value (values "duration"),
           :on-change handle-change,
           :on-blur handle-blur}]
         [render-error "duration"]
         [:input
          {:type "unit",
           :name "unit",
           :placeholder "units"
           :value (values "unit"),
           :on-change handle-change,
           :on-blur handle-blur}]
         [render-error "unit"]]
        [:button.button
         {:disabled (and (seq errors) (> attempted-submissions 0))}
         "Valider"]
        [:button.button
         {:on-click #(do
                       (.stopPropagation %)
                       (.preventDefault %)
                       (rf/dispatch [:hide-editor]))}
         "Annuler"]]))])

(defn toggle-editor
  [current-editor selected-editor]
  (rf/dispatch (if-not (= current-editor selected-editor)
                 [:display-editor selected-editor]
                 [:hide-editor])))

(defn task-list-ui
  []
  (let [coming-days 1
        next-tasks (rf/subscribe [::subs/next-tasks coming-days])
        other-tasks (rf/subscribe [::subs/other-tasks coming-days])
        show-more? (rf/subscribe [::subs/show-more])
        editor @(rf/subscribe [::subs/displayed-editor])]
    [:div
     [:div#refresh
      [:button
       {:on-click #(rf/dispatch [:refresh-tasks/query])}
       "Tout rafraîchir"]
      [:button
       {:on-click #(rf/dispatch [:reorder-entries])}
       "Réordoner"]
      [:button
       {:on-click #(toggle-editor editor :repeated)}
       "Nouvelle tâche"]]
     (case editor
       :repeated [repeated-task-form]
       nil)
     [:ul.tasks
      (map task-entry-ui @next-tasks)]
     (when (seq @other-tasks)
       [:<>
        [:div#load-more
         [:button
          {:on-click #(rf/dispatch [:toggle-show-more])}
          (str (if @show-more? "Cacher" "Afficher")
               " les tâches à venir ("
               (count @other-tasks)
               ")")]]
        (when @show-more?
          [:ul.tasks
           (map task-entry-ui @other-tasks)])])]))

(defn main-panel []
  (let [api (rf/subscribe [::subs/api])
        tasks (rf/subscribe [::subs/tasks])
        status (rf/subscribe [::subs/status])]
    [:div
     [connection-status-ui @api @status]
     (cond
       (seq @tasks) [task-list-ui]
       (= @status :in-progress) [:p "Loading..."]
       :else [:p "No tasks"])]))
