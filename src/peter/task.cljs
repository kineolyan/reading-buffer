(ns peter.task
  (:require [cljs-time.core :as dt]
            [cljs-time.local :as dl]
            [cljs-time.coerce :as time-coerce]))

(def current-time (time-coerce/to-local-date (dl/local-now)))

(defn days-diff
  [start end]
  (dt/in-days (dt/interval start end)))

(defn timestamp->local-date
  [timestamp]
  (-> timestamp
      time-coerce/from-long
      time-coerce/to-local-date))

(defn compute-days-to-target
  "Computes the number of days from now to the due date for a given task"
  [task]
  (let [due-date (-> task :due-date timestamp->local-date)]
    (if (dt/before? current-time due-date)
      (days-diff current-time due-date)
      (- (days-diff due-date current-time)))))
