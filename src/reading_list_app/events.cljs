(ns reading-list-app.events
  (:require
   [clojure.string :as str]
   [re-frame.core :as rf]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [day8.re-frame.http-fx]
   [cljs-time.core :as time-core]
   [cljs-time.coerce :as time-coerce]
   [cljs-time.format :as time-format]
   [ajax.core :as ajax]
   [shared.fauna-date :as fd]
   [shared.fauna-fx :as f]
   [shared.fauna-query :as fq]
   [reading-list-app.db :as db]))

(def fdb-time-format (time-format/formatters :date-time))

(defn ->fdb-time
  [timestamp]
  (some->> timestamp
           (time-coerce/from-long)
           (time-format/unparse fdb-time-format)))

(comment
  (->fdb-time (js/Date.now))
  (->fdb-time nil))

(defn format-remote-entry
  [{:keys [_id url title type created_at read_at]}]
  (->> #:entry{:id _id
               :url url
               :title title
               :type (or type "STANDARD")
               :created-at (fd/datetime->timestamp created_at)
               :read-at (fd/datetime->timestamp read_at)}
       (filter (comp some? second))
       (into {})))

(rf/reg-event-fx
 :load-entries/success
 (fn-traced
  load-entries
  [{:keys [db]} [_ {:keys [items] :as data}]]
  (let [formatted (mapv format-remote-entry items)
        completed (into (:entries db) formatted)]
    {:db (assoc db
                :query-result data
                :entries completed
                :connected? :yes)})))

(rf/reg-event-db
 :load-entries/error
 (fn-traced
  [db [_ result]]
  (assoc db
         :query-errors result
         :connected? :no)))

(rf/reg-event-fx
 :load-entries/query
 (fn-traced
  query-entries
  [{:keys [db] :as cofx} [_ cursor]]
  (merge
   {:db (update db :entries #(if cursor % []))}
   (f/fauna-query cofx {:read {:collection "entries"
                               :page-size 50}
                        :on-success [:load-entries/success]
                        :on-failure [:load-entries/error]}))))

(defn read-clipboard []
  (if (.-clipboard js/navigator)
    (-> (.-clipboard js/navigator)
        (.readText)
        (.then (fn [clipboard-text] (str/trim clipboard-text)))
        (.catch (fn [error]
                  (js/Promise.reject (str "Unable to read clipboard content:" error)))))
    (js/Promise.reject "Clipboard API not supported in this browser")))

(rf/reg-event-fx
 :clipboard/load
 (fn-traced
  [_ _]
  (-> (read-clipboard)
      (.then
       (fn [content]
         (when (or (str/starts-with? content "http://")
                   (str/starts-with? content "https://"))
           (rf/dispatch [:clipboard/save content])))
       js/console.error))
  {}))

(rf/reg-event-db
 :clipboard/save
 (fn-traced
  [db [_ value]]
  (assoc-in db [:new-entry :entry/url] value)))

(rf/reg-event-fx
 ::initialize-db
 [(rf/inject-cofx :local-store-token db/fauna-key)]
 (fn-traced
  initialize-app
  [{token :local-store-token} _]
  (let [existing-token? (not (str/blank? token))]
    {:db (assoc db/default-db
                :token token
                :connected? (if existing-token? :in-progress :no))
     :fx [(when existing-token? [:dispatch [:load-entries/query]])
          [:dispatch [:clipboard/load]]]})))

(comment
  (rf/dispatch [::initialize-db])
  (rf/dispatch [:load-entries/query]))

(rf/reg-event-db
 :set-token
 [(rf/path :token)
  (rf/after (partial f/token->local-store db/fauna-key))]
 (fn-traced
  [_db [_ v]]
  v))

(comment
  (rf/dispatch [:set-token "Xalkd4kjU"])
  (f/local-store->token db/fauna-key))

(rf/reg-event-db
 :edit-token
 (fn-traced
  [db _]
  (assoc db :token-form {:editing? true})))

(rf/reg-event-fx
 :use-new-token
 (fn-traced
  [{:keys [db]} [_ value]]
  {:db (-> db
           (update-in [:token-form :editing?] (constantly false))
           (assoc :connected? :in-progress))
   :fx [[:dispatch [:set-token value]]
        [:dispatch [:load-entries/query]]]}))

(rf/reg-event-db
 :mark-connected
 (fn-traced
  [db _]
  (assoc db :connected? :yes)))

(defn current-time
  []
  (js/Date.now))

(defn create-entry-from-db
  [{:keys [url title type]} f-id]
  {:pre [(not (str/blank? url))
         (not (str/blank? type))]}
  (let [id (f-id)
        date (current-time)]
    #:entry{:id id
            :url (str/trim url)
            :title (str/trim title)
            :type (str/trim type)
            :created-at date}))

(defn add-new-entry-to-db
  [db new-entry]
  (let [pending-entry (assoc new-entry :entry/saved? :in-progress)]
    (update-in db [:entries] conj pending-entry)))

(defn reset-entry-form
  [db]
  (update-in db [:new-entry] assoc :entry/url "" :entry/title "" :entry/type "STANDARD"))

(rf/reg-event-fx
 :add-entry
 (fn-traced
  [{:keys [db] :as cofx} [_ definition]]
  (let [new-entry (create-entry-from-db definition current-time)
        local-id (:entry/id new-entry)]
    (merge
     {:db (-> db
              (add-new-entry-to-db new-entry)
              (reset-entry-form))}
     (f/fauna-query cofx {:create {:collection "entries"
                                   :datum {:url (:entry/url new-entry)
                                           :title (:entry/title new-entry)
                                           :type (:entry/type new-entry)
                                           :created_at (-> new-entry :entry/created-at ->fdb-time fq/Time.)}}
                          :on-success [::http-entry-inserted local-id]
                          :on-failure [::http-entry-not-inserted local-id]})))))

(comment
  (def db @re-frame.db/app-db)
  (def cofx {:db db})
  (def new-entry (create-entry-from-db db current-time)))

(defn mark-saved-entry
  [entries local-id remote-id]
  (mapv
   #(if (= (:entry/id %) local-id)
      (assoc %
             :entry/id remote-id
             :entry/saved? :yes)
      %)
   entries))

(rf/reg-event-db
 ::http-entry-inserted
 (fn-traced
  [db [_ local-id result]]
  (let [remote-id (get-in result [:data :createEntry :_id])]
    (-> db
        (assoc :query-result result)
        (update-in [:entries] mark-saved-entry local-id remote-id)))))

(defn flag-remote-error
  [l id]
  (mapv
   #(if (= (:entry/id %) id)
      (assoc % :entry/saved? :error)
      %)
   l))

(rf/reg-event-db
 ::http-entry-not-inserted
 (fn-traced
  [db [_ local-id errors]]
  (-> db
      (assoc :query-errors errors)
      (update-in [:entries] flag-remote-error local-id))))

(defn delete-entry
  [l id]
  (mapv
   #(if (= (:entry/id %) id)
      (assoc %
             :entry/read-at (current-time)
             :entry/saved? :in-progress)
      %)
   l))

(rf/reg-event-fx
 :delete-entry
 (fn-traced
  [{:keys [db] :as cofx} [_ id]]
  (merge {:db (update-in db [:entries] delete-entry id)}
         (f/fauna-query cofx {:update {:target {:collection "entries" :id id}
                                       :values {:read_at (-> (current-time) ->fdb-time fq/Time.)}}
                              :on-success [::http-entry-read id]
                              :on-failure [::http-entry-read-failure id]}))))

; TODO it would be better to read the data from the result and set it as the date
; or to pass the same date, instead of calling twice `current-time`
(rf/reg-event-db
 ::http-entry-read
 (fn-traced
  [db [_ id _result]]
  (update-in db [:entries] mark-saved-entry id id)))

(rf/reg-event-db
 ::http-entry-read-failure
 (fn-traced
  [db [_ id errors]]
  (-> db
      (assoc :query-errors errors)
      (update-in [:entries] flag-remote-error id))))

(rf/reg-event-db
 :switch-to-list
 (fn-traced
  [db [_ k]]
  (update db :selected-list #(if (= % k) nil k))))

(rf/reg-event-db
 :delete-read/request
 (fn-traced
  [db _]
  (update-in db [:forms] assoc :confirm/delete-read :?)))

(rf/reg-event-db
 :delete-read/abort
 (fn-traced
  [db _]
  (update-in db [:forms] dissoc :confirm/delete-read)))

(rf/reg-event-fx
 :delete-read/confirm
 (fn-traced
  [{:keys [db] :as cofx} _]
  (merge
   {:db (update-in db [:forms] dissoc :confirm/delete-read)}
   (f/fauna-query cofx {:query (fq/Call (fq/Function "delete_read_entries") 50 nil nil)
                        :on-success [::http-read-deleted]
                        :on-failure [::http-read-not-deleted]}))))

(rf/reg-event-db
 ::http-read-deleted
 (fn-traced
  [db [_ result]]
  (tap> [:result result])
  (def result* result)
  (let [data (aget result "data")
        _ (tap> [:data data])
        ids (->> data
                 (map f/read-item)
                 (map :_id)
                 (into #{}))]
    (update db :entries (partial filterv #(-> % :entry/id ids not))))))

(comment
  (let [result result*]
    (let [data (aget result "data")
          ids (->> data
                   (map f/read-item)
                   (map :_id))]
      ids)))

(rf/reg-event-db
 ::http-read-not-deleted
 (fn-traced
  [db [_ errors]]
  (assoc db :query-errors errors)))

(rf/reg-event-db
 :change-page
 (fn-traced
  [db [_ page]]
  (assoc db :page page)))

(defmulti get-newsletter-urls (fn [id _] id))
(defmethod get-newsletter-urls :twir
  [id db]
  (let [url-fn (get-in db [:newsletters id :url-fn])
        today (time-core/today)
        days (map #(time-core/minus today (time-core/days %)) (range 7))]
    (tap> [url-fn days])
    (map url-fn days)))

(comment
  (let [today (time-core/today)
        days (map #(time-core/minus today (time-core/days %)) (range 7))
        date-fmt (time-format/formatters :date)]
    (map
     #(time-format/unparse date-fmt %)
     days))
  (->
   (time-core/today)
   (time-core/minus (time-core/days 1)))
  (get-newsletter-urls
    :twir
    @re-frame.db/app-db))

(rf/reg-event-fx
 :newsletters/fetch
 (fn-traced
  [{:keys [db]} [_ id]]
  (let [query-uris (get-newsletter-urls id db)]
    (tap> [:urls query-uris])
    {:db (assoc-in db [:newsletters id :status] :loading)
     :fx [[:dispatch [:newsletters/load id query-uris]]]})))

(rf/reg-event-fx
 :newsletters/load
 (fn-traced
  [_ [_ id urls]]
  {:http-xhrio {:uri (first urls)
                :method :get
                :on-success [:newsletters/result :success id]
                :on-failure [:newsletters/result :failure id (rest urls)]
                :response-format (ajax/text-response-format)}}))

(rf/reg-event-fx
 :newsletters/result
 (fn-traced
  [{:keys [db]} [_ status id & others]]
  (case status
    :success
    (let [[content] others]
      {:db (update-in db [:newsletters id] merge {:content content
                                                  :status :loaded})})
    :failure
    (let [[urls content] others]
      (if (seq urls)
        {:fx [[:dispatch [:newsletters/load id urls]]]}
        (-> db
            (assoc :query-errors [id content])
            (assoc-in [:newsletters id :status] :failed)))))))

(comment
  (rf/dispatch [:newsletters/fetch :twir]))

(rf/reg-event-db
 :newsletters/display
 (fn-traced
  [db [_ id]]
  (assoc db :selected-newsletter id)))

(rf/reg-event-db
 :newsletters/pick-link
 (fn-traced
  [db [_ {:keys [target label]}]]
  (assoc-in db [:picked-links target] label)))

(rf/reg-event-db
 :newsletters/unpick-link
 (fn-traced
  [db [_ target]]
  (update db :picked-links dissoc target)))

(rf/reg-event-fx
 :newsletters/save-picked-links
 (fn-traced
  [{:keys [db]} _]
  (let [link-commands (for [[target label] (:picked-links db)]
                        [:add-entry {:url target
                                     :title label
                                     :type "STANDARD"}])]
    {:db (-> db
             (assoc :page :bookmarks)
             (dissoc :selected-newsletter)
             (assoc :picked-links {}))
     :fx (mapv #(vector :dispatch %) link-commands)})))

(comment
  (rf/dispatch [:newsletters/save-picked-links]))
