(ns reading-list-app.views.components)

(defn ui-accordion
  [{:keys [items selected on-select]}]
  [:ul.accordion
   (map
    (fn [{:keys [id content title]}]
      (let [selected? (= id selected)]
        [:li.accordion-item
         {:key id
          :className (when selected? "is-active")}
         [:a.accordion-title {:href "#" :on-click #(on-select id)} title]
         [:div.accordion-content
          (if selected? {:style {:display "block"}} {})
          content]]))
    items)])
