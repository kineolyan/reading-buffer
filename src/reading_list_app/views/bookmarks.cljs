(ns reading-list-app.views.bookmarks
  (:require
   [clojure.string :as str]
   [re-frame.core :as rf]
   [cljs-time.coerce :as time-coerce]
   [cljs-time.format :as time-format]
   [reading-list-app.subs :as subs]
   [reading-list-app.views.components :as cpn]
   [fork.re-frame :as fork]))

(def std-date-format (time-format/formatters :date))

(defn format-date
  [timestamp]
  (let [datetime (time-coerce/from-long timestamp)]
    (time-format/unparse std-date-format datetime)))

(defn truncate-str
  "Truncates the string s to retain only n chars, completing with ..."
  [v n]
  (if (< (count v) n)
    v
    (str (subs v 0 (- n 3)) "...")))

(defn ui-entry [{:entry/keys [id title url created-at read-at] :as entry}]
  (let [title-elt (if-not (str/blank? title) title url)
        link-elt [:a
                  {:href url :target "_blank" :rel "noreferrer"}
                  (truncate-str url 80)]]
    [:li.callout
     {:key (str id)}
     [:button.close-button
      {:aria-label "Mark entry as read"
       :type "button"
       :on-click #(rf/dispatch [:delete-entry id])}
      [:span {:aria-hidden "true"} "×"]]
     [:h4.entry-title link-elt]
     [:p
      [:span.entry-subs
       (try
         (if (some? read-at)
           (str "(read: " (format-date read-at) ")")
           (str "(created: " (format-date created-at) ")"))
         (catch js/Error _
           (tap> entry)
           "oops"))]
      " "
      title-elt]]))

(defn ui-entry-list [{sub :subscription}]
  (let [entries (rf/subscribe [sub])]
    [:div.grid-x
     [:ul.entry-list.cell
      (map ui-entry @entries)]]))

(defn create-new-entry
  [values]
  ; only checking type in pre
  (rf/dispatch [:add-entry {:url (get values "url")
                            :title (get values "title" "")
                            :type (get values "type")}]))

(defn prevent-button-event
  [e]
  (doto e
    (.preventDefault)
    (.stopPropagation)))

(defn ui-entry-type-btn
  [{:keys [current label color on-selected] :as entry}]
  (let [{entry-type :type entry-key :key} entry
        classes (cond-> ["button"]
                  (not= current entry-type) (conj "hollow")
                  (some? color) (conj color))]
    [:input
     {:key entry-key
      :type "button"
      :className (str/join " " classes)
      :value label
      :on-click #(do (prevent-button-event %)
                     (on-selected entry-type))}]))

(def entry-types
  [{:type "STANDARD" :label "std" :color nil}
   {:type "SERIE" :label "serie" :color "warning"}
   {:type "VIDEO" :label "video" :color "warning"}
   {:type "STASH" :label "stash" :color "secondary"}])

(def youtube-patterns
  [#"https?://(?:www.)youtube.com/.*"
   #"https?://youtu.be/.*"])

(defn youtube-link?
  [url]
  (boolean
   (some #(re-matches % url) youtube-patterns)))

(comment
  (youtube-link? "https://www.youtube.com/watch?v=ghGvFcg6GEQ")
  (youtube-link? "https://youtu.be/ghGvFcg6GEQ"))

(defn validate-entry
  [values]
  (cond-> {}
    (str/blank? (values "url"))
    (assoc "url" "No URL provided")
    (not (contains? (set (map :type entry-types)) (values "type")))
    (assoc "type" "No type selected")
    (and (str/blank? (values "title"))
         (= (values "type") "VIDEO"))
    (assoc "title" "Title required for videos")))

(defn on-detected-type
  [event f]
  (let [value (-> event .-target .-value str/trim)]
    (when (youtube-link? value)
      (f "VIDEO"))))

(defn new-entry-default-values
  [{:entry/keys [url title type] :or {url "" title "" type "STANDARD"}}]
  {"type" type
   "url" url
   "title" title})

(defn ui-new-entry []
  (let [new-entry (rf/subscribe [::subs/new-entry])]
    [fork/form
     {:initial-values (new-entry-default-values @new-entry)
      :validation validate-entry
      :on-submit (fn [{:keys [values reset]}]
                   (create-new-entry values)
                   (reset {:values (new-entry-default-values {})}))
      :prevent-default? true}
     (fn [{:keys
           [values
            set-values
            touched
            errors
            handle-change
            handle-blur
            handle-submit]}]
       (let [render-error (fn [x]
                            (when (and (touched x) (get errors x))
                              [:div.cell (get errors x)]))]
         [:form
          {:on-submit handle-submit}
          [:div.grid-x.grid-padding-x
           [:div.cell
            [:div.input-group
             [:div.input-group-button
              [:div#new-entry-type.button-group
               [:input
                {:type "button"
                 :className "button secondary"
                 :value "rel."
                 :on-click #(rf/dispatch [:load-entries/query])}]]]
             [:input
              {:type "text",
               :name "url",
               :placeholder "URL to add to the list"
               :className "input-group-field"
               :value (values "url")
               :on-change (fn [e]
                            (handle-change e)
                            (on-detected-type e #(set-values {"type" %})))
               :on-blur handle-blur}]
             [:div.input-group-button
              [:div#new-entry-type.button-group
               (map
                (fn [entry]
                  (vector ui-entry-type-btn
                          (assoc entry
                                 :current (values "type")
                                 :key (:type entry)
                                 :on-selected #(set-values {"type" %}))))
                entry-types)]]]]
           (when-not (str/blank? (values "url"))
             [:div.cell
              [:input
               {:type "text",
                :name "title",
                :placeholder "Optional title for the entry"
                :value (values "title"),
                :on-change handle-change
                :on-blur handle-blur}]])
           [render-error "url"]
           [render-error "title"]
           [render-error "type"]
           [:div.cell
            [:button.button.expanded
             {:disabled (seq errors)}
             "Add"]]]]))]))

(defn delete-button
  [{:keys [title ask confirm abort confirm?]}]
  (if-not confirm?
    [:div
     [:a.button.expanded {:on-click (fn [_] (ask))} title]]
    [:div.expanded.button-group
     [:a.button.alert
      {:on-click (fn [_] (confirm)) :style {:flex-grow 10}}
      title]
     [:a.hollow.button.secondary {:on-click (fn [_] (abort))} "Abort"]]))

(defn ui-read-entries
  []
  (let [confirm-delete? (rf/subscribe [::subs/confirm-delete-read])]
    [:div
     [delete-button {:title "Delete read entries"
                     :confirm? @confirm-delete?
                     :ask #(rf/dispatch [:delete-read/request])
                     :confirm #(rf/dispatch [:delete-read/confirm])
                     :abort #(rf/dispatch [:delete-read/abort])}]
     [ui-entry-list {:subscription ::subs/read-entries}]]))

(defn ui-accordion-title
  [n title]
  [:b
   title
   " "
   [:span.badge
    (if (pos? n) {:className "success"} {})
    n]])

(def panels
  [{:id :unread :title "Articles" :sub ::subs/unread-entries}
   {:id :series :title "Long-to-read series" :sub ::subs/series}
   {:id :videos :title "Videos / Conferences" :sub ::subs/videos}
   {:id :stash :title "-- stash --" :sub ::subs/stash}
   {:id :pending :title "Changes in progress" :sub ::subs/pending-entries}])

(defn ui-root
  []
  (let [selected @(rf/subscribe [::subs/selected-list])
        stats @(rf/subscribe [::subs/entry-stats])]
    [:<>
     [ui-new-entry]
     [cpn/ui-accordion
      {:selected selected
       :on-select #(rf/dispatch [:switch-to-list %])
       :items (concat
               (for [{:keys [id title sub]} panels]
                 {:id id
                  :title (ui-accordion-title (get stats sub) title)
                  :content [ui-entry-list {:subscription sub}]})
               [{:id :read
                 :title (ui-accordion-title (get stats ::subs/read-entries) "Read entries")
                 :content [ui-read-entries]}])}]]))
