(ns reading-list-app.views.newsletter
  (:require
   [clojure.string :as str]
   [cljs.pprint :as pprint]
   [reagent.core :as reagent]
   [re-frame.core :as rf]
   [cljs-time.coerce :as time-coerce]
   [cljs-time.format :as time-format]
   [reading-list-app.subs :as subs]
   [fork.re-frame :as fork]))

(defn -request-newsletter
  [id]
  (rf/dispatch [:newsletters/fetch id]))

(defn -display-newsletter
  [id]
  (rf/dispatch [:newsletters/display id]))

(defn ui-picked-links
  []
  (let [links @(rf/subscribe [::subs/picked-links])]
    [:div.picked-links
     "Selected links"
     [:ul
      (doall
       (for [[target label] links]
         [:li.seleted-link
          {:key target}
          [:a.button.tiny.hollow.alert.delete-link
           {:href "#"
            :on-click #(rf/dispatch [:newsletters/unpick-link target])}
           [:i.fa-solid.fa-trash]]
          label]))]
     (when (seq links)
       [:button.button.primary.expanded.success.hollow
        {:on-click #(rf/dispatch [:newsletters/save-picked-links])}
        "Add links to reading-buffer"])]))

(defn -render-link
  [label target]
  (let [selected @(rf/subscribe [::subs/picked-links])
        checked? (contains? selected target)
        _ (when checked? (tap> [selected target]))
        on-change #(let [state (-> % .-target .-checked)]
                     (rf/dispatch (if state
                                    [:newsletters/pick-link
                                     {:target target :label label}]
                                    [:newsletters/unpick-link target])))]
    [:span
     {:key target}
     [:a
      {:href target
       :target "_blank"}
      label]
     [:input.link-adder {:type "checkbox"
                         :checked (boolean checked?)
                         :on-change on-change}]]))

(def -link-pattern #"\[([^\]]+?)]\(([^\)]+?)\)")

(defn -render-text
  [content]
  (let [parts (str/split content -link-pattern)
        link-tags (->> (partition 3 parts)
                       (map rest)
                       (map (partial apply -render-link)))
        text-parts (->> (partition-all 3 parts)
                        (map first))]
    (doall
     (concat
      (interleave text-parts link-tags)
      (drop (count link-tags) text-parts)))))

(comment
  (def value
    "[Designing a GC in Rust](https://manishearth.github.io/blog/2015/09/01/designing-a-gc-in-rust/)")
  (str/split "a [1](2) b [3](4) c" -link-pattern)
  (str/split value -link-pattern)
  (-render-text value)
  (-render-text "a [1](2) b [3](4) c"))

(defn -render-title
  [value]
  (let [pounds (count (filter #(= \# %) value))
        title-level (min 6 pounds)
        title-tag (str "h" title-level)]
    [(keyword title-tag) (str/replace-first value #"\s*#+" "")]))

(defn -render-list
  [list-sign block]
  (let [escaped-sign (if (some #{"*" "+" "?"} [list-sign])
                       (str "\\" list-sign)
                       list-sign)
        items (str/split block (re-pattern (str "\\s*" escaped-sign "\\s+")))]
    [:ul
     (doall
      (for [[item i] (map vector (drop 1 items) (range))]
        [:li {:key i} (-render-text item)]))]))

(comment
  (str/split
   "* abc
   - cd"
   #"\s*[*\-]\s+"))

(defn -render-markdown-markup
  [block]
  (cond
    (str/starts-with? block "#")
    (-render-title block)

    (str/starts-with? block "- ")
    (-render-list "-" block)
    (str/starts-with? block "* ")
    (-render-list "*" block)

    :else
    [:p (-render-text block)]))

(defn -clean-html-comments
  [content]
  (str/replace
   content
   #"(?sm)<!-{2,}[\s\S]+?-{2,}>"
   ""))

(comment
  (-clean-html-comments "ab<!-- ignoredline -->cd")
  (-clean-html-comments "ab<!-- ignored\nlines -->cd"))

(defn complete-blocks
  [content]
  (str/replace
    content
    #"(?sm)(?<=\n)(#{1,}[^\n]+)(?=\n[^\n])"
    "$1\n"))

(comment
  (complete-blocks "ab\ncd")
  (complete-blocks "ab\n## title\n coming next")
  (complete-blocks "ab\n## title\n\n not need"))

(defn -render-markdown
  [content]
  (let [content (-> content -clean-html-comments complete-blocks)
        blocks (str/split content #"\n{2,}")]
    (doall
     (for [[block pos] (map vector blocks (range))]
       [:div {:key pos} (-render-markdown-markup block)]))))

(comment
  (def content (-> @re-frame.db/app-db
                   :newsletters
                   :twir
                   :content)))

(defn ui-root
  []
  [:<>
   [:p "Pick links in the page"]
   [:ul
    (for [{:keys [id title status]} @(rf/subscribe [::subs/newsletters])
          :let [disabled? (not (= status :loaded))]]
      [:li.newsletter-item
       {:key id}
       [:span.newsletter-title title]
       [:button.button.primary
        {:on-click #(-request-newsletter id)}
        "Load"]
       [:button.button.warning
        {:on-click #(-display-newsletter id)
         :className (when disabled? "disabled")
         :disabled (boolean disabled?)}
        "Display"]])]
   (when-let [{:keys [title content]} @(rf/subscribe [::subs/selected-newsletter])]
     [:<>
      [ui-picked-links]
      [:hr]
      [:h3 title]
      [:div (-render-markdown content)]])])
