(ns reading-list-app.views.login
  (:require [cljs.pprint :as pprint]
            [re-frame.core :as rf]
            [fork.re-frame :as fork]
            [reading-list-app.subs :as subs]))

(defn truncate-str
  "Truncates the string s to retain only n chars, completing with ..."
  [v n]
  (if (< (count v) n)
    v
    (str (subs v 0 (- n 3)) "...")))

(defn ui-title
  [token]
  (let [token-value (if (some? token)
                      (str "\"" (truncate-str token 23) "\"")
                      "nil")
        token-class (if (some? token) "defined" "nil")]
    [:h1.cell.app-title
     [:span.paren "("]
     [:span.read-fn "read-buffer "]
     [:span.token
      {:className token-class
       :on-click #(rf/dispatch [:edit-token])}
      token-value]
     [:span.paren ")"]]))

(defn -get-token-from-url!
  []
  (-> js/window
      .-location
      .-search
      (subs 1)
      js/URLSearchParams.
      (.get "token")))

(defn ui-header []
  (let [token @(rf/subscribe [::subs/token])
        token-form @(rf/subscribe [::subs/token-form])]
    [:div.grid-x
     [ui-title token]
     (when (or (nil? token) (token-form :editing?))
       [fork/form {:initial-values {"token" (or token (-get-token-from-url!))}
                   :on-submit (fn [x]
                                (rf/dispatch [:use-new-token (get-in x [:values "token"])]))
                   :prevent-default? true}
        (fn [{:keys [values
                     handle-change
                     handle-blur
                     handle-submit]}]
          [:form.cell.input-group
           {:on-submit handle-submit}
           [:input {:type "text"
                    :name "token"
                    :value (values "token")
                    :className "input-group-field"
                    :placeholder "Set the token to connect to a reading list"
                    :on-change handle-change
                    :on-blur handle-blur}]
           [:div.input-group-button
            [:button.button "Save"]]])])]))

(defn ui-form
  []
  (let [errors @(rf/subscribe [::subs/errors])]
    [:div.grid-container
     [ui-header]
     (when (seq errors)
       [:div.grid-x
        [:div.cell
         [:pre (with-out-str (pprint/pprint errors))]]])]))

(defn ui-connecting-panel
  []
  [:div.grid-container
   [ui-header]
   [:div.grid-x
    [:p.cell "Connecting ..."]]])
