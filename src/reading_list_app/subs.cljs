(ns reading-list-app.subs
  (:require [re-frame.core :as rf]))

(rf/reg-sub
 ::token
 (fn [db]
   (:token db)))

(rf/reg-sub
 ::token-form
 (fn [db]
   (:token-form db)))

(rf/reg-sub
 ::connected
 (fn [db]
   (:connected? db)))

(defn read?
  [entry]
  (-> entry :entry/read-at some?))

(defn unread-entry-of-type
  "Creates a predicate matching an unread entry of a given type"
  [t]
  (fn [entry]
    (and (not (read? entry))
         (= (:entry/type entry) t))))

(comment
  (def entry {:entry/id 142074
              :entry/type "STANDARD"})
  ((unread-entry-of-type "SERIE") entry))

(rf/reg-sub
 ::unread-items
 (fn [db [_ t]]
   (filterv
    (unread-entry-of-type (or t "STANDARD"))
    (:entries db))))

(rf/reg-sub
 ::series
 :<- [::unread-items "SERIE"]
 (fn [entries _]
   entries))

(rf/reg-sub
 ::unread-entries
 :<- [::unread-items "STANDARD"]
 (fn [entries _]
   entries))

(rf/reg-sub
 ::stash
 :<- [::unread-items "STASH"]
 (fn [entries _]
   entries))

(rf/reg-sub
 ::videos
 :<- [::unread-items "VIDEO"]
 (fn [entries _]
   entries))

(rf/reg-sub
 ::read-entries
 (fn [db]
   (filterv
    read?
    (:entries db))))

(def pending-states #{:in-progress :error})
(defn pending?
  [entry]
  (contains? pending-states (:entry/saved? entry)))

(rf/reg-sub
 ::pending-entries
 (fn [db]
   (filterv pending? (:entries db))))

(def categories
  [::unread-entries
   ::series
   ::stash
   ::read-entries
   ::pending-entries
   ::videos])

(rf/reg-sub
 ::entry-stats
 (fn [_query-v]
   (mapv #(rf/subscribe [%]) categories))
 (fn [subscriptions _]
   (->> subscriptions
        (mapv (fn [k vs] [k (count vs)]) categories)
        (into {}))))

(rf/reg-sub
 ::new-entry
 (fn [db]
   (:new-entry db)))

(rf/reg-sub
 ::selected-list
 (fn [db]
   (:selected-list db)))

(rf/reg-sub
 ::confirm-delete-read
 (fn [db]
   (= (-> db :forms :confirm/delete-read) :?)))

(rf/reg-sub
 ::errors
 (fn [db]
   (filter
    some?
    [(:query-errors db)])))

(rf/reg-sub
 ::page
 (fn [db]
   (db :page)))

(rf/reg-sub
 ::newsletters
 (fn [db]
   (map
    (fn [[id newsletter]]
      (merge {:id id}
             (select-keys newsletter [:title :status])))
    (:newsletters db))))

(rf/reg-sub
 ::newsletter
 (fn [db [_ id]]
   (if-let [entry (get-in db [:newsletters id])]
     entry
     {:status :not-found})))

(rf/reg-sub
 ::selected-newsletter
 (fn [db]
   (when-let [id (:selected-newsletter db)]
     (select-keys
      (get-in db [:newsletters id])
      [:title :content]))))

(rf/reg-sub
  ::picked-links
  (fn [db]
    (db :picked-links)))
