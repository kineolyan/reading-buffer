(ns reading-list-app.db
  (:require [cljs-time.format :as time-format]))

(defn -timestamp [year month day]
  (.getTime (js/Date. year (dec month) day)))

(def -some-entries-for-tests
  [#:entry{:id 1
           :title "Abc"
           :url "https://ab.c"
           :created-at (-timestamp 2021 6 12)}
   #:entry{:id 2
           :title "alsjd"
           :url "https://startpage.com"
           :created-at (-timestamp 2021 7 4)}])

(def newsletters
  {:twir {:url-fn (fn [date]
                    (let [formatter (time-format/formatters :date)]
                      (str
                       "https://raw.githubusercontent.com/rust-lang/this-week-in-rust/master/content/"
                       (time-format/unparse formatter date)
                       "-this-week-in-rust.md")))
          :title "This Week In Rust"}})

(def default-db
  {:page :bookmarks
   :new-entry {:entry/url ""
               :entry/title ""
               :entry/type "STANDARD"}
   :token-form {:editing? false}
   :selected-list :unread
   :entries [] ; We can also use -some-entries-for-tests
   :newsletters newsletters
   :picked-links {}
   :forms {}})

(def fauna-key "reading-fauna-token")
