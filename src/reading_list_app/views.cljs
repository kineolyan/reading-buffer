(ns reading-list-app.views
  (:require
   [reagent.core :as reagent]
   [re-frame.core :as rf]
   [reading-list-app.subs :as subs]
   [reading-list-app.views.bookmarks :as bookmarks]
   [reading-list-app.views.newsletter :as newsletter]
   [reading-list-app.views.login :as login]))

(defn ui-page-buttons
  [page]
  [:div.button-group.expanded
   [:a.button
    {:on-click #(rf/dispatch [:change-page :bookmarks])
     :className (if (= page :bookmarks) "" "hollow")}
    "Bookmarks"]
   [:a.button.success
    {:on-click #(rf/dispatch [:change-page :newsletters])
     :className (if (= page :newsletters) "" "hollow")}
    "Newsletters"]])

(defn std-panel
  []
  (let [page @(rf/subscribe [::subs/page])]
    [:<>
     [:div.grid-container
      [login/ui-header]
      [ui-page-buttons page]]
     (case page
       :bookmarks
       [:div.grid-container
        [bookmarks/ui-root]]
       :newsletters
       [:div.grid-container
        [newsletter/ui-root]])]))

(defn main-panel []
  (let [connected? (rf/subscribe [::subs/connected])]
    (case @connected?
      :yes [std-panel]
      :no [login/ui-form]
      :in-progress [login/ui-connecting-panel])))
