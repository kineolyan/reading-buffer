(ns gitdesk.subs
  (:require [re-frame.core :as rf]
            [cljs-time.core :as t]
            [cljs-time.coerce :as tc]
            [gitdesk.db :as db]))

;; General state

(rf/reg-sub
 ::route
 (fn [db]
   (get db :route :dashboard)))

(rf/reg-sub
 ::api
 (fn [db]
   (merge (:config db)
          {:token (:token db)
           :github-user (:user db)
           :usage (:usage db)})))

;; Github collections

(rf/reg-sub
 ::loading-prs
 (fn [db]
   (-> db :loading vals set)))

(rf/reg-sub
 ::prs
 (fn [db]
   (:prs db)))

(defn authored?
  [user pr]
  (= user (:author pr)))

(defn assigned?
  [user pr]
  (some #{user} (:assignees pr)))

(defn smart-assigned?
  "Checks that the assigned PR is not authored by the current user."
  [user pr]
  (and (assigned? user pr)
       (not (authored? user pr))))

(defn create-comparator
  [rules]
  (let [xy-rules (map (fn [[direction & fs]]
                        (let [getter (apply comp fs)
                              fs [(comp getter first) (comp getter second)]]
                          (if (= direction :asc) fs (reverse fs))))
                      rules)
        x-rules (apply juxt (map first xy-rules))
        y-rules (apply juxt (map second xy-rules))]
    (fn [x y]
      (compare (x-rules [x y]) (y-rules [x y])))))

(defn sort-result
  [{rules :order :or {rules []}} xs]
  (if (seq rules)
    (sort (create-comparator rules) xs)
    xs))

(rf/reg-sub
 ::my-prs
 :<- [::api]
 :<- [::prs]
 (fn [[api prs] [_ options]]
   (->> prs
        (filter (partial authored? (:github-user api)))
        (sort-result options))))

(comment
  (def rules [[:desc :a] [:desc :b]])
  (def xs [{:a 1 :b 2} {:a 2 :b 1} {:a 1 :b 3}])
  ((apply juxt (map identity [(comp #(get-in % '(:a)) first)
                              (comp #(get-in % '(:b)) second)]))
   (reverse xs))
  (map (fn [[d & ks]] [:dbg d ks]) rules)
  (sort (create-comparator [[:desc - :a] [:desc :b]]) xs)
  (rf/subscribe [::my-prs {:order [[:asc :draft?]
                                   [:desc tc/to-long :created-at]]}]))

(rf/reg-sub
 ::reviews
 (fn [db]
   (or (:reviews db) #{})))

(rf/reg-sub
 ::my-reviews
 :<- [::prs]
 :<- [::reviews]
 (fn [[prs reviews] [_ options]]
   (->> prs
        (filter #(contains? reviews (:id %)))
        (sort-result options))))

(rf/reg-sub
 ::my-assignments
 :<- [::api]
 :<- [::prs]
 (fn [[api prs] [_ options]]
   (->> prs
        (filter (partial smart-assigned? (:github-user api)))
        (sort-result options))))

(rf/reg-sub
 ::my-mentions
 (fn [db [_ options]]
   (sort-result options (:mentions db))))

(rf/reg-sub
 ::work-stats
 :<- [::my-prs]
 :<- [::my-reviews]
 :<- [::my-assignments]
 :<- [::my-mentions]
 (fn [[prs reviews assignments mentions] _]
   {:authored (count prs)
    :to-review (count reviews)
    :assigned (count assignments)
    :addressed (count mentions)}))

(comment
  (def db @re-frame.db/app-db)
  (-> db :user)
  (filter (partial assigned? "fabiencelier") (:prs db))
  (filter (partial smart-assigned? "fabiencelier") (:prs db))
  (filter (partial authored? "fabiencelier") (:prs db))
  (-> db :prs first)
  (count *1)
  (rf/subscribe [::api])
  (-> *1 deref :mates)
  (rf/subscribe [::prs])
  (rf/subscribe [::my-prs])
  (rf/subscribe [::my-reviews])
  (tap> @(rf/subscribe [::team-stats])))

(defn days
  [date]
  (t/in-days
   (t/interval
    date
    (t/now))))

(rf/reg-sub
 ::aging-prs
 :<- [::prs]
 (fn [prs]
   (filter #(<= 15 (days (:created-at %))) prs)))

(rf/reg-sub
 ::conflicting-prs
 :<- [::prs]
 (fn [prs]
   (filter #(<= 15 (:comment-count %)) prs)))

(rf/reg-sub
 ::difficult-prs
 :<- [::prs]
 (fn [prs [_ options]]
   (->> prs
        (filter #(<= 10 (:commit-count %)))
        (filter #(= (db/approved-pr? %) :ok))
        (sort-result options))))

(defn count-last-failures
  [pr]
  (->> (:commits pr)
       (map #(vector (db/green-commit? %) %))
       (drop-while #(= :unknown (first %)))
       (take-while #(= :ko (first %)))
       (count)))

(rf/reg-sub
 ::stuck-prs
 :<- [::prs]
 (fn [prs [_ options]]
   (->> prs
        (filter db/approved-pr?)
        (filter (comp some? :commits))
        (filter #(<= 10 (count-last-failures %)))
        (sort-result options))))

(rf/reg-sub
 ::monitor-stats
 :<- [::aging-prs]
 :<- [::conflicting-prs]
 :<- [::stuck-prs]
 :<- [::difficult-prs]
 :-> (fn [colls]
       (zipmap
        [:aging :conflicting :stuck :difficult]
        (map count colls))))

(rf/reg-sub
 ::merged-prs
 (fn [db [_ options]]
   (sort-result options (:merged db))))

(rf/reg-sub
 ::merging-prs
 :<- [::prs]
 (fn [prs [_ options]]
   (->> prs
        (filter #(and (not (:draft? %))
                      (= (:mergeable %) "MERGEABLE")
                      (:auto-merge? %)
                      (every? db/approving-review? (:reviews %))))
        (sort-result options))))

(rf/reg-sub
 ::train-stats
 :<- [::merging-prs]
 :<- [::merged-prs]
 :-> (fn [[merging merged]]
       {:merging (count merging)
        :merged (count merged)}))

(rf/reg-sub
 ::authored-prs
 :<- [::prs]
 (fn [prs [_ {:keys [user] :as options}]]
   (->> prs
        (filter (partial authored? user))
        (sort-result options))))

(rf/reg-sub
 ::assigned-prs
 :<- [::prs]
 (fn [prs [_ {:keys [user] :as options}]]
   (->> prs
        (filter (partial smart-assigned? user))
        (sort-result options))))

(rf/reg-sub
 ::team-stats
 :<- [::api]
 :<- [::prs]
 (fn [[{:keys [mates]} prs] _]
   (mapv
    #(hash-map :nickname %
               :authored (count (sequence
                                 (filter (partial authored? %))
                                 prs))
               :drafted (count (sequence
                                (comp
                                 (filter (partial authored? %))
                                 (filter (partial :draft?)))
                                prs))
               :assigned (count (sequence
                                 (filter (partial smart-assigned? %))
                                 prs)))
    mates)))
