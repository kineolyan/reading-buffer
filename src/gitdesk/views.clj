(ns gitdesk.views
  (:require [clojure.zip :as zip]))

(defn zip-map
  " Map f over every node of the zipper.
    The function received has the form (f node-value loc),
    the node value and its location"
  [f loc]
  (loop [z loc]
    (if (zip/end? z)
      (zip/root z) ; perhaps you can call zip/seq-zip or zip/vector-zip?
      (recur (zip/next (zip/edit z f z))))))

#_(defn genetist
    [tree & _]
    (zip-map
     (fn [node _loc]
       (if (= 'children node)
         [:a :new :entry]
         node))
     (zip/vector-zip tree)))

(defn zip-cat
  [tree item sub]
  {:pre [seq? sub]}
  (loop [z (zip/vector-zip tree)]
    (if (zip/end? z)
      (zip/root z)
      (if (= item (zip/node z))
        (let [new-range (vec (concat (zip/lefts z) sub (zip/rights z)))
              new-loc (-> z (zip/up) (zip/replace new-range))]
          (recur (-> new-loc zip/down zip/next)))
        (recur (zip/next z))))))

(comment
  (zip-cat [1 [2 'c 3] 4 'c [10 [20 30 'c]] 40] 'c [:s 8 5 6 :e]))

(defn genetist-lab
  ([tree children]
   (genetist-lab tree 'chidren children))
  ([tree marker children]
   (zip-cat tree marker children)))

(comment
  (genetist-lab
   [:div
    [:p "first"]
    [:p 1 'c 2]
    [:p "next"]
    'c]
   'c
   [[:a "link"]]))

(defmacro genetist
  [tree]
  (list
    `genetist-lab
    (list `let '[children 'children] tree)
    (quote 'children)
    'children))

(comment
  (let [children [[:a "link"]]]
    (genetist
       [:div
        [:p "first"]
        [:p 1 children 2]
        [:p "next"]
        children])))
