(ns gitdesk.reactor
  (:require [clojure.zip :as zip]))


(defmacro genetist
  [tree]
  (list
    `genetist-lab
    (list `let '[children 'children] tree)
    (quote 'children)
    'children))

(comment
  (let [children [[:a "link"]]]
    (genetist
       [:div
        [:p "first"]
        [:p 1 children 2]
        [:p "next"]
        children])))
