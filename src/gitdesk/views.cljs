(ns gitdesk.views
  (:require [reagent.core :as r]
            [clojure.string :as str]
            [cljs-time.coerce :as time-coerce]
            [cljs-time.format :as time-format]
            [clojure.core.match :refer-macros [match]]
            [re-frame.core :as rf]
            [fork.re-frame :as fork]
            [gitdesk.config :as cfg]
            [gitdesk.db :as db]
            [gitdesk.subs :as subs]
            [gitdesk.reactor :refer-macros [genetist]]))

(defn date->html
  [value]
  (time-format/unparse (time-format/formatters :date) value))

(defn datetime->html
  [value]
  (time-format/unparse (time-format/formatters :date-time-no-ms) value))

(defn time->html
  [value]
  (time-format/unparse (time-format/formatters :hour-minute) value))

(defn github-label->html
  [value]
  (-> value
      (str/replace #":[\w]+:" "")
      (str/trim)))

(defn fa-icon-ui
  [icon & {:keys [weight class classes props] :or {weight "solid" class "" classes [] props {}}}]
  [:span.icon
   props
   [:i {:class (str "fa-" weight " fa-" icon " " class " " (str/join " " classes))}]])

(defn arrow-link
  [route]
  [fa-icon-ui
   "play"
   :props {:class "has-text-success inner-link"
           :on-click #(rf/dispatch [:goto route])}])

(defn validate-config
  [{:strs [github-token projects mates refresh]}]
  (let [max-refresh (* 24 60)]
    (cond-> {}
      (str/blank? github-token)
      (assoc "github-token" "Empty token")
      (str/blank? projects)
      (assoc "projects" "Empty list of projects")
      (str/blank? mates)
      (assoc "mates" "Empty list of team members")
      (not (pos-int? refresh))
      (assoc "refresh" "Value too low. Positive value required")
      (< max-refresh refresh)
      (assoc "refresh" (str "Value too high. Max = " max-refresh)))))

(defn input->names
  [value]
  (vec (re-seq #"[\w/\-\._]+" value)))

(defn save-config!
  [{:strs [github-token fauna-token projects mates refresh]}]
  (rf/dispatch [:update-config {:token github-token
                                :fauna-token fauna-token
                                :projects (input->names projects)
                                :mates (input->names mates)
                                :refresh-rate {:min (js/parseInt refresh)}}]))

(defn connection-badge
  [value]
  (case value
    :failed [:span "(down ⭕)"]
    :in-progress [:span "(...)"]
    [:span value " (up ✅)"]))

(defn form-error-ui
  [{:keys [errors touched]} k]
  (when (and (touched k) (get errors k))
    [:p.help.is-danger (get errors k)]))

(defn form-item-ui
  [{:keys [label]} & children]
  (genetist
   [:div.field.is-horizontal
    [:div.field-label
     [:label.label label]]
    [:div.field-body children]]))

(defn form-section-ui
  [{:keys [title]} & children]
  (genetist
   [:form.block
    [form-item-ui
     {:label ""}
     [:h3.title.is-5 title]]
    children]))

(defn form-input-ui
  [{:keys [handle-change handle-blur values] :as form-context}
   {:keys [k label hint]}]
  [form-item-ui
   {:label label}
   [:div.control
    [:div.field
     [:input.input
      {:type "text"
       :name k
       :placeholder (or hint (str "Enter a " label))
       :value (values k)
       :on-change handle-change
       :on-blur handle-blur}]]
    [form-error-ui form-context k]]])

(defn form-text-ui
  [{:keys [handle-change handle-blur values] :as form-context}
   {:keys [k label hint rows] :or {rows 5}}]
  [form-item-ui
   {:label label}
   [:div.field
    [:div.control
     [:textarea.textarea
      {:name k
       :placeholder hint
       :rows rows
       :value (values k)
       :on-change handle-change
       :on-blur handle-blur}]]
    [form-error-ui form-context k]]])

(defn config-ui
  []
  (let [api @(rf/subscribe [::subs/api])]
    [:<>
     [:div.block
      [:p "Github connection: " [connection-badge (or (:github-user api) :failed)]]
      (when-let [usage (:usage api)]
        [:p.has-text-grey
         "Github usage"
         [:span.icon
          {:on-click #(rf/dispatch [:github-connection/check])}
          [:i.fa-solid.fa-magnifying-glass-chart]]
         ": " (-> api :usage :consumed) "/" (-> usage :limit)
         " (reset at " (-> usage :reset-at time->html) ")"])
      [:p "FaunaDB connection: " [connection-badge (or (:fauna-up api) :failed)]]]
     [fork/form
      {:initial-values {"github-token" (get api :token "")
                        "fauna-token" (get api :fauna-token "")
                        "projects" (str/join "\n" (get api :projects ""))
                        "mates" (str/join "\n" (get api :mates ""))
                        "refresh" (get-in api
                                          [:refresh-rate :min]
                                          (:min db/default-refresh-frequency))}
       :validation validate-config
       :on-submit (fn [x] (save-config! (:values x)))
       :prevent-default? true}
      (fn [{:keys [errors
                   values
                   handle-change
                   handle-blur
                   handle-submit
                   attempted-submissions] :as form-context}]
        [:div
         {:on-submit handle-submit}
         [form-section-ui
          {:title "Connection"}
          [form-input-ui
           form-context
           {:k "github-token" :label "Github token" :hint "Enter a token"}]
          [form-input-ui
           form-context
           {:k "fauna-token" :label "FaunaDb token" :hint "Enter a token"}]]
         [form-section-ui
          {:title "Tracked elements"}
          [form-text-ui
           form-context
           {:k "projects" :label "Projects" :hint "List of projects to follow"}]
          [form-text-ui
           form-context
           {:k "mates" :label "Team members" :hint "Team members to follow"}]]
         [form-section-ui
          {:title "Experience"}
          [form-item-ui
           {:label "Refresh rate"}
           [:div.field.has-addons
            [:p.control
             [:input.input
              {:type "number"
               :name "refresh"
               :placeholder "Selects the interval at which PRs are automatically refreshed"
               :value (values "refresh")
               :on-change handle-change
               :on-blur handle-blur}]]
            [:p.control
             [:a.button.is-static "mins"]]]
           [form-error-ui form-context "refresh"]]]
         [:form.block
          [:div.field.is-horizontal
           [:div.field-label] ; to comply with the layout
           [:div.field-body
            [:div.field
             [:div.control
              [:button.button
               {:disabled (and (seq errors) (pos? attempted-submissions))}
               "Save"]]]]]]])]]))

(defn pr-category-ui
  [title n]
  [:div.box.level
   [:div.level-left
    [:div.level-item
     [:p.subtitle.is-3 title]]]
   [:div.level-right
    [:div.level-item
     [:span.tag n]]]])

(def work-defs
  [{:id :authored
    :label "Authored"
    :route [:authored 'me]}
   {:id :to-review
    :label "To review"
    :route [:to-review 'me]}
   {:id :assigned
    :label "Assigned"
    :route [:assigned 'me]}
   {:id :addressed
    :label "Addressed"
    :route [:addressed 'me]}])

(defn my-work
  []
  (let [work-sub @(rf/subscribe [::subs/work-stats])]
    [:div.container.block
     (concat)
     [:h2.title.is-3 "My work"]
     [:div.level
      (for [{:keys [id label route]} work-defs
            :let [disabled-attrs (if-not route {:class "has-text-grey-light"} {})]]

        [:div.level-item.has-text-centered
         {:key id}
         [:div
          [:p.heading
           disabled-attrs
           label
           (when route [arrow-link route])]
          [:p.title
           disabled-attrs
           (get work-sub id 0)]]])]]))

(def monitored-items
  [:stuck :aging :conflicting :difficult])

(def monitored-defs
  {:stuck {:label "Stuck"
           :route [:monitoring :stuck]}
   :aging {:label "Aging"
           :route [:monitoring :aging]}
   :conflicting {:label "Conflicting"
                 :route [:monitoring :conflicting]}
   :difficult {:label "Difficult"
               :route [:monitoring :difficult]}})

(defn monitored-prs
  []
  (let [monitor-sub @(rf/subscribe [::subs/monitor-stats])
        {:keys [merging merged]} @(rf/subscribe [::subs/train-stats])]
    [:div.container.block
     (concat)
     [:h2.title.is-3 "Monitoring"]
     [:div.columns
      [:p.column.is-one-quarter "Merge train"]
      [:p.column.is-one-quarter
       "Merging ... "
       [:span.tag.is-info merging]
       [arrow-link [:train :merging]]]
      [:p.column.is-one-quarter
       "Just merged "
       [:span.tag.is-success.is-light merged]
       [arrow-link [:train :merged]]]]
     [:div.level
      (for [item monitored-items]
        (let [{:keys [label route]} (get monitored-defs item)
              disabled-attrs (if-not route {:class "has-text-grey-light"} {})]
          [:div.level-item.has-text-centered
           {:key item}
           [:div
            [:p.heading
             disabled-attrs
             label
             (when route [arrow-link route])]
            [:p.title
             disabled-attrs
             (get monitor-sub item)]]]))]]))

(defn create-bar-chart-ui
  [{:keys [max-size values padding] :or {padding 0}}]
  (let [padding-percent (* 100 (/ padding max-size))
        sizes (map :count values)
        widths (->> sizes
                    (reductions +)
                    (map #(* 100 (/ % max-size))))]
    [:svg
     {:width "100%"
      :height "25px"}
     (concat
      [:g]
      (reverse
       (mapv
        (fn [{:keys [color] className :class} width]
          [:rect
           {:key className
            :x (str padding-percent "%")
            :width (str width "%")
            :fill color
            :class className
            :height "100%"}])
        values
        widths)))]))

(defn my-team
  []
  [:div.container.block
   [:h2.title.is-3 "Team work"]
   [:p.subtitle.has-text-weight-light "(authored / assigned)"]
   (let [stats @(rf/subscribe [::subs/team-stats])
         [max-authored max-assigned] (map #(->> stats (map %) (reduce max)) [:authored :assigned])
         max-size (+ max-authored max-assigned)]
     (for [{:keys [nickname authored assigned drafted]} stats]
       [:div.block.columns
        {:key nickname}
        [:div.column.is-one-quarter
         [:span "@" nickname]
         [:span.has-text-weight-light " (" authored " / " assigned ")"]
         [arrow-link [:mate nickname]]]
        [:div.column.is-three-quarters
         (when (pos? max-size)
           (let [under-review (- authored drafted)]
             [create-bar-chart-ui
              {:max-size max-size
               :padding (- max-authored authored)
               :values [{:class "authored" :count under-review :color "#fdd77c"}
                        {:class "drafted" :count drafted :color "#a17202"}
                        {:class "assigned" :count assigned :color "#a0d5ff"}]}]))]]))])

(defn home-btn
  []
  [:button.button.is-rounded
   {:on-click #(rf/dispatch [:goto :dashboard])}
   [fa-icon-ui "house"]])

(defn setting-btn
  []
  [:button.button.is-rounded
   {:on-click #(rf/dispatch [:goto :settings])}
   [fa-icon-ui "gear"]])

(defn load-btn
  []
  (let [status @(rf/subscribe [::subs/loading-prs])
        api @(rf/subscribe [::subs/api])
        disabled? (nil? (:github-user api))
        loading? (and (seq status) (contains? status :loading))
        errors? (and (seq status) (contains? status :ko))
        classes [(if errors? "is-danger" "is-info")
                 (if (or disabled? loading?)
                   (when loading? "is-loading")
                   "is-light")]]
    [:button.button.is-rounded
     (merge
      {:on-click #(rf/dispatch [:load-prs])
       :class (str/join " " (keep identity classes))}
      (when (or disabled? loading?)
        {:disabled true}))
     [fa-icon-ui "rotate"]]))

(defn loading-bar
  []
  loading-bar
  (let [status @(rf/subscribe [::subs/loading-prs])]
    (when (contains? status :loading)
      [:div
       [:progress.progress
        {:class (if (contains? status :ko) "is-danger" "is-info")}]])))

(defn dashboard-ui
  []
  [:<>
   [my-work]
   [my-team]
   [monitored-prs]])

(defn settings-ui
  []
  [:div.container
   [:h2.title.is-2 "Parameters"]
   [config-ui]])

(defn approval-class
  [approval]
  (case approval
    :ok "has-text-success"
    :ko "has-text-danger"
    :unknown "has-text-grey-light"))

(defn pr-buttons-ui
  []
  [:<>
   [fa-icon-ui "star"]
   [fa-icon-ui "star" :weight "regular"]
   [fa-icon-ui "bell"]
   [fa-icon-ui "bell-slash" :weight "regular"]])

(defn pr-ui
  [{pr :data}]
  (let [hover (r/atom false)]
    (fn [_]
      [:div.box
       {:on-mouse-enter #(reset! hover true)
        :on-mouse-leave #(reset! hover false)}
       [:div.columns
        [:p.column.is-three-quarters.py-0
         [:a
          {:href (:url pr) :target "_blank"
           :class (if (:draft? pr) "has-text-grey" "has-text-info")}
          [:span
           [fa-icon-ui "code-pull-request"]
           " #"
           (:number pr)]]
         " "
         (:title pr)]
        [:p.column.is-one-quarter.has-text-right.py-0
         [:span.has-text-grey (:base pr)]
         " "
         [:span.has-text-grey-light (date->html (:created-at pr))]]]
       [:div.columns.reduced-row
        [:p.column.pt-0
         (when-let [status (db/green-pr? pr)]
           [fa-icon-ui "heart-circle-check" :class (approval-class status)])
         (when-let [status (db/approved-pr? pr)]
           [fa-icon-ui "circle-check" :class (approval-class status)])
         " "
         [:span.pr-author (:author pr)]
         [fa-icon-ui "right-long"]
         " { "
         [:<> (for [x (:assignees pr)]
                [:span.pr-assignee {:key x} x " "])]
         "}"]
        [:p.column.is-narrow.has-text-right.pt-0
         (if-not @hover
           (for [{:keys [name color]} (:labels pr)]
             [:span.px-1
              {:key name
               :style {:color (str "#" color)}}
              (github-label->html name)])
           [pr-buttons-ui])]]])))

(defn pr-view-ui
  [{:keys [sub]}]
  ; We do the filtering and stats for drafs locally as we don't expect a lot of PRs nor drafts
  ; A pure use of reframe would move this to the subscription
  ; We could indeed have a subscription returning the values and stats
  (let [prs @(rf/subscribe sub)
        {:keys [show-drafts?]} @(rf/subscribe [::subs/api])
        shown-prs (if show-drafts? prs (remove :draft? prs))]
    [:div.container.block
     [:h2.title (-> sub first name)]
     (let [pr-count (count prs)
           shown-count (count shown-prs)
           hidden-count (- pr-count shown-count)]
       [:h4.subtitle pr-count " PRs in the list"
        [:i.has-text-weight-light
         " (including " hidden-count " draft" (when (< 1 hidden-count) "s") ")"]])
     [:label.checkbox
      [:input  {:type "checkbox"
                :checked show-drafts?
                :on-change #(rf/dispatch [:toggle-drafts (-> % .-target .-checked)])}]
      " Show drafts"]
     (for [pr shown-prs] [pr-ui {:key (:id pr) :data pr}])]))

(def pr-order
  [[:asc :draft?]
   [:asc time-coerce/to-long :created-at]])

(defn routed-view
  []
  (let [route @(rf/subscribe [::subs/route])]
    (match [route]
      [:settings] [settings-ui]
      [:dashboard] [dashboard-ui]
      [[:authored 'me]] [pr-view-ui {:sub [::subs/my-prs {:order pr-order}]}]
      [[:to-review 'me]] [pr-view-ui {:sub [::subs/my-reviews {:order pr-order}]}]
      [[:assigned 'me]] [pr-view-ui {:sub [::subs/my-assignments {:order pr-order}]}]
      [[:addressed _]] [pr-view-ui {:sub [::subs/my-mentions {:order pr-order}]}]
      [[:mate nickname]] [:<>
                          [pr-view-ui {:sub [::subs/authored-prs
                                             {:user nickname
                                              :order pr-order}]}]
                          [pr-view-ui {:sub [::subs/assigned-prs
                                             {:user nickname
                                              :order pr-order}]}]]
      [[:monitoring :conflicting]] [pr-view-ui {:sub [::subs/conflicting-prs
                                                      {:order [[:asc time-coerce/to-long :created-at]]}]}]
      [[:monitoring :aging]] [pr-view-ui {:sub [::subs/aging-prs
                                                {:order [[:asc time-coerce/to-long :created-at]]}]}]
      [[:monitoring :difficult]] [pr-view-ui {:sub [::subs/difficult-prs
                                                    {:order [[:desc time-coerce/to-long :created-at]]}]}]
      [[:monitoring :stuck]] [pr-view-ui {:sub [::subs/stuck-prs
                                                {:order [[:desc time-coerce/to-long :created-at]]}]}]
      [[:train :merging]] [pr-view-ui {:sub [::subs/merging-prs
                                             {:order [[:asc time-coerce/to-long :update-at]]}]}]
      [[:train :merged]] [pr-view-ui {:sub [::subs/merged-prs
                                            {:order [[:desc time-coerce/to-long :updated-at]]}]}]
      :else [:<>
             [:div.container.notification.is-danger.is-light
              [:button.delete
               {:on-click #(rf/dispatch [:goto :dashboard])}]
              [:p "Unknown route " (pr-str route)]]
             [dashboard-ui]])))

(comment
  (rf/dispatch [:goto :dashboard]))

(defn header-ui
  []
  [:div.container
   [:div.level
    [:div.level-item
     [:div.field.is-grouped
      [:p.control [home-btn]]
      (when cfg/debug?
        ;; Display all buttons on both sides as reframe-10x may hide some
        [:<>
         [:p.control [setting-btn]]
         [:p.control [load-btn]]])]]
    [:div.level-item.has-text-centered
     [:h1.title.is-1 "Github Control Desk"]]
    [:div.level-item
     [:div.field.is-grouped
      [:p.control [setting-btn]]
      [:p.control [load-btn]]]]]
   [loading-bar]])

(defn main-panel []
  [:<>
   [:section.section
    [header-ui]
    [routed-view]]])
