(ns gitdesk.db)

;; :stuck       ; approved but errors in the last commits
;; :aging       ; created a long time ago
;; :conflicting ; with too many comments
;; :difficult   ; with too many commits and not approved yet

(def default-db
  {:mentions []
   :prs []
   :merged []})

(def storage-key "gitdesk-token")
(def config-key "gitdesk-config")

(def default-refresh-frequency {:min 60})

(defn green-commit?
  [commit]
  (let [statuses (-> commit :status vals set)]
    (cond
      (empty? statuses) :unknown
      (= statuses #{:success}) :ok
      (some #{:pending} statuses) :unknown
      (some #{:expected} statuses) :ko
      (some #{:error :failure} statuses) :ko)))

(defn green-pr?
  [pr-data]
  (when-let [commits (:commits pr-data)]
    (green-commit? (first commits))))

(defn approved-pr?
  [pr-data]
  (when-let [reviews (:reviews pr-data)]
    (let [statuses (-> reviews vals set)]
      (cond
        (empty? statuses) :unknown
        (= statuses #{:approved}) :ok
        (some #{:changes-requested} statuses) :ko
        :else :unknown))))

(defn approving-review?
  "Tests if a given review from a PR approves it"
  [_pr-review]
  ;; TODO
  true)

(defn ok-commit?
  "Tests if a given commit passes all its checks"
  [commit-data]
  (every? #{:success} (-> commit-data :status vals)))
