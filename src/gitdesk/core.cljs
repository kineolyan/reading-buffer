(ns gitdesk.core
  (:require
   [reagent.dom :as rdom]
   [re-frame.core :as re-frame]
   [shared.interval-fx :as interval-fx]
   [gitdesk.events :as events]
   [gitdesk.views :as views]
   [gitdesk.config :as config]))

(defn dev-setup []
  (when config/debug?
    (println "dev mode")))

(defn ^:dev/after-load mount-root []
  (re-frame/clear-subscription-cache!)
  (let [root-el (.getElementById js/document "app")]
    (rdom/unmount-component-at-node root-el)
    (rdom/render [views/main-panel] root-el)))

(defn init []
  (interval-fx/reset-fx!)
  (re-frame/dispatch-sync [::events/initialize-db])
  (dev-setup)
  (mount-root))
