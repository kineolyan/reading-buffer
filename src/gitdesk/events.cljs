(ns gitdesk.events
  (:require
   [clojure.string :as str]
   [clojure.set :as set]
   [day8.re-frame.http-fx]
   [day8.re-frame.tracing :refer-macros [fn-traced defn-traced]]
   [cljs-time.core :as time-core]
   [cljs-time.coerce :as time-coerce]
   [cljs-time.format :as time-format]
   [re-frame.core :as rf]
   [venia.core :as v]
   [shared.fauna-fx :as f]
   [shared.graphql :as g]
   [shared.storage-fx :as store]
   [gitdesk.db :as db]
   [gitdesk.config :as cfg]))

(comment
  (defn get-db [] @re-frame.db/app-db))

;;; utility functions

(defn parse-date
  [s]
  (time-format/parse
   (time-format/formatters :date-time-no-ms)
   s))

(defn -read-cursor
  "Reads a GraphQL cursor from the query result.
  Use `:order :next` or `:order :prev` to select the wanted iteration (default `:next`).
  Returns `nil` when the end of the cursor is reached."
  [result & {:keys [order] :or {order :next}}]
  (when-let [page-info (:pageInfo result)]
    (case order
      :next (when (:hasNextPage page-info) (:endCursor page-info))
      :prev (when (:hasPreviousPage page-info) (:startCursor page-info)))))

(defn read-next-cursor
  [result]
  (-read-cursor result :order :next))

;;; Navigation events

(rf/reg-event-db
 :goto
 (fn-traced
  [db [_ route]]
  (assoc db :route route)))

;;; Graphql query definitions

(defn viewer-info-query
  []
  (v/graphql-query
   {:venia/queries
    [[:viewer
      [:login]]
     [:rateLimit
      [:limit :remaining :resetAt]]]}))

(defn pr-state-fragment
  [{:keys [history reviews] :or {history 20 reviews 10}}]
  (concat
   [:mergeable
    [:autoMergeRequest [:enabledAt]]]
   (when (pos? reviews)
     [[:reviews
       {:last reviews}
       [[:nodes
         [:state
          :updatedAt
          [:author [:login]]]]]]])
   (when (pos? history)
     [[:commits
       {:last history}
       [:totalCount
        [:nodes
         [[:commit
           [:authoredDate
            :pushedDate
            [:status
             [[:contexts
               [:state
                :context]]]]]]]]]]])))

(defn pr-info-fragment
  [config]
  (concat
   [:id
    :number
    [:author [:login]]
    :isDraft
    :title
    :url
    :createdAt
    :updatedAt
    :baseRefName
    [:repository [:nameWithOwner]]
    [:assignees
     {:first 20}
     [[:nodes
       [:login]]]]
    [:labels
     {:first 20}
     [[:nodes [:name :color]]]]
    [:comments [:totalCount]]]
   (pr-state-fragment config)))

(defn project-prs-query
  "Collects open PRs for a given project"
  [{:keys [n after history] :or {n 25 history 20}}]
  ; from queries/repo-prs.gql
  (v/graphql-query
   {:venia/variables [{:variable/name "owner"
                       :variable/type :String!}
                      {:variable/name "name"
                       :variable/type :String!}]
    :venia/queries
    [[:repository
      {:name :$name
       :owner :$owner}
      [[:pullRequests
        (merge {:first n
                :states 'OPEN}
               (when after {:after after}))
        [[:pageInfo [:hasNextPage :endCursor]]
         [:nodes
          (pr-info-fragment {:history history})]]]]]]}))

(defn pr-idents-fragment
  [{:keys [fragment-name] :or {fragment-name "prNodes"}}]
  {:fragment/name fragment-name
   :fragment/type :PullRequest
   :fragment/fields (vec (concat
                          [:id]
                          (when cfg/debug? [:number [:repository [:nameWithOwner]]])))})

(defn user-mentions-query
  [{:keys [n after user] :or {n 25}}]
  {:pre [(not (str/blank? user))]}
  ; from queries/mentionning.gql
  (v/graphql-query
   {:venia/queries
    [[:search
      (merge
       {:query (str "is:open is:pr archived:false mentions:" user)
        :type 'ISSUE
        :first n}
       (when after {:after after}))
      [[:pageInfo [:hasNextPage :endCursor]]
       [:nodes [:fragment/prNodes]]]]]
    :venia/fragments
    [{:fragment/name "prNodes"
      :fragment/type :PullRequest
      :fragment/fields (pr-info-fragment {:history 1})}]}))

(defn review-requests-query
  [{:keys [n after user] :or {n 25}}]
  {:pre [(not (str/blank? user))]}
  (v/graphql-query
   {:venia/queries
    [[:search
      (merge
       {:query (str "is:open is:pr archived:false user-review-requested:" user)
        :type 'ISSUE
        :first n}
       (when after {:after after}))
      [[:pageInfo [:hasNextPage :endCursor]]
       [:nodes [:fragment/prs]]]]]
    :venia/fragments
    [(pr-idents-fragment {:fragment-name "prs"})]}))

(defn merged-prs-query
  [{:keys [repository n] :or {n 25}}]
  {:pre [(not (str/blank? repository))]}
  (let [; use a large offset in dev? to have enough content for this section
        offset (if cfg/debug? (time-core/years 2) (time-core/days 3))
        start-date (time-core/minus (time-core/now) offset)
        date-str (time-format/unparse (time-format/formatters :date) start-date)]
    (v/graphql-query
     {:venia/queries
      [[:search
        {:query (str "is:closed is:pr repo:" repository " archived:false merged:>="  date-str)
         :type 'ISSUE
         :first n}
        [[:nodes [:fragment/prNodes]]]]]
      :venia/fragments
      [{:fragment/name "prNodes"
        :fragment/type :PullRequest
        :fragment/fields (pr-info-fragment {:history 0
                                            :reviews 0})}]})))

(comment
  (project-prs-query {})
  (user-mentions-query {:n 10 :user "me"})
  (merged-prs-query {:n 5 :repository "Kineolyan/project-tz-io"})
  (review-requests-query {:user "Kineolyan"}))

;;; Config events

(rf/reg-event-fx
 :update-config
 (fn-traced
  [{:keys [db]} [_ payload]]
  (let [token (:token payload)
        config (dissoc payload :token)]
    (f/token->local-store db/storage-key token)
    {:db (assoc db
                :config config
                :token token)
     ::store/storage [:write db/config-key config]
     :fx [[:dispatch [:github-connection/check]]]})))

(rf/reg-event-fx
 :toggle-drafts
 (fn-traced
  [{:keys [db]} [_ value]]
  (let [ks [:config :show-drafts?]
        new-db (if (nil? value)
                 (update-in db ks not)
                 (assoc-in db ks value))]
    {:db new-db
     ::store/storage [:write db/config-key (:config new-db)]})))

;;; Connection events

(rf/reg-event-fx
 :github-connection/check
 (fn-traced
  [cofx _]
  (let [query (merge (g/build-xhrio-query
                      (-> cofx :db :token)
                      {:uri "https://api.github.com/graphql"
                       :query (str "query " (viewer-info-query))
                       :variables {}})
                     {:on-success [:github-connection/success]
                      :on-failure [:github-connection/failure]})]
    {:db (assoc (:db cofx) :user :in-progress)
     :http-xhrio query})))

(rf/reg-event-db
 :github-connection/success
 [rf/unwrap]
 (fn-traced
  [db response]
  (let [username (get-in response [:data :viewer :login])
        {:keys [limit remaining] reset-at :resetAt} (get-in response [:data :rateLimit])]
    (assoc db :user username
           :usage {:limit limit
                   :consumed (- limit remaining)
                   :reset-at (parse-date reset-at)}))))

(rf/reg-event-db
 :github-connection/failure
 [rf/unwrap]
 (fn-traced
  [db response]
  (assoc db
         :user :failed
         :debug-error response)))

;;; PR listing events

(rf/reg-event-fx
 :project-prs/fetch
 (fn-traced
  [cofx [_ {:keys [owner repository after] :as payload}]]
  (when-not (= :loading (get-in cofx [:db :loading [owner repository]]))
    (let [query (merge (g/build-xhrio-query
                        (-> cofx :db :token)
                        {:uri "https://api.github.com/graphql"
                         :query (str "query projectprs" (project-prs-query {:n 25 :after after}))
                         :variables {"owner" owner
                                     "name" repository}})
                       {:on-success [:project-prs/result payload]
                        :on-failure [:project-prs/result payload]})]
      {:db (assoc-in (:db cofx) [:loading [owner repository]] :loading)
       :http-xhrio query}))))

(defn gql->keyword
  [value]
  (-> value
      (str/lower-case)
      (str/replace #"_" "-")
      (keyword)))

(defn gql-enum
  [values]
  (->> values
       (map
        (juxt identity gql->keyword))
       (into {})))

(def commit-states
  (gql-enum ["EXPECTED" ; Status is expected.
             "ERROR"    ; Status is errored.
             "FAILURE"  ; Status is failing.
             "PENDING"  ; Status is pending.
             "SUCCESS"  ; Status is successful.
             ]))

(def review-states
  (gql-enum ["PENDING"           ; A review that has not yet been submitted.
             "COMMENTED"         ; An informational review.
             "APPROVED"          ; A review allowing the pull request to merge.
             "CHANGES_REQUESTED" ; A review blocking the pull request from merging.
             "DISMISSED"         ; A review that has been dismissed.
             ]))

(defn format-commit
  [data]
  {:authored-at (parse-date (get-in data [:authoredDate]))
   :pushed-at (some-> (get-in data [:authoredDate]) parse-date)
   :status (->> (get-in data [:status :contexts])
                (map (juxt :context #(get commit-states (:state %) :unknown)))
                (into {}))})

(defn format-reviews
  "Formats all PR reviews to only keep the last reviews for each user.
  Returns the reviews as a map `author -> review`"
  [reviews]
  (->> reviews
       (sort-by (comp time-coerce/to-long parse-date :updatedAt))
       (map #(vector (-> % :author :login)
                     (get review-states (:state %))))
       (filter (fn [[_author state]] (some #{state} [:approved :changes-requested])))
       (into {})))

(defn format-pr
  [data]
  (merge (select-keys data [:id :number :title :url])
         {:author (get-in data [:author :login])
          :draft? (get-in data [:isDraft])
          :created-at (parse-date (get-in data [:createdAt]))
          :updated-at (parse-date (get-in data [:updatedAt]))
          :assignees  (->> (get-in data [:assignees :nodes])
                           (map :login)
                           (sort)
                           (into []))
          :labels (into [] (get-in data [:labels :nodes]))
          :repository (get-in data [:repository :nameWithOwner])
          :base (get-in data [:baseRefName])
          :state {:mergeable (get-in data [:mergeable])
                  :auto-merge? (some? (get-in data [:autoMergeRequest :enabledAt]))}
          :comment-count (get-in data [:comments :totalCount])}
         (when-let [commit-data (get-in data [:commits :nodes])]
           {:commit-count (get-in data [:commits :totalCount])
            :commits (->> commit-data
                          (mapv (comp format-commit :commit))
                          (remove (comp nil? :pushed-at))
                          (sort-by (comp time-coerce/to-long :authored-at) >)
                          vec)})
         (when-let [review-data (get-in data [:reviews :nodes])]
           {:reviews (format-reviews review-data)})))

(defn format-pr-idents
  [{:keys [id number] {:keys [nameWithOwner]} :repository}]
  (merge
   {:id id}
   (when cfg/debug? {:number number
                     :nameWithOwner nameWithOwner})))

(defn interesting-pr?
  [db pr-data]
  (let [contributor? (into #{(-> db :user)} (-> db :config :mates))]
    (or (contains? contributor? (:author pr-data))
        (some contributor? (:assignees pr-data)))))

(comment
  (let [db (get-db)]
    (filter
     #(interesting-pr? db %)
     (:prs (get-db)))))

(def *result (atom nil))

(rf/reg-event-fx
 :project-prs/result
 (defn-traced handle-pr-result
   [{:keys [db]} [_ {:keys [owner repository] :as payload} result]]
   (reset! *result result)
   (if (contains? result :data)
     (let [nodes (get-in result [:data :repository :pullRequests :nodes])
           prs (map format-pr nodes)
           prs-of-interest (filter #(interesting-pr? db %) prs)
           end-cursor (read-next-cursor (get-in result [:data :repository :pullRequests]))]
       (merge
        {:db (-> (cond-> db
                   cfg/debug? (assoc :debug-success result))
                 (update :prs into prs-of-interest)
                 (assoc-in [:loading [owner repository]] :ok))}
        (when end-cursor
          {:fx [[:dispatch [:project-prs/fetch (assoc payload :after end-cursor)]]]})))
     {:db (-> db
              (assoc-in [:loading [owner repository]] :ko)
              (assoc :debug-error result))})))

(rf/reg-event-fx
 :user-mentions/fetch
 (fn-traced
  [cofx [_ {:keys [after]}]]
  (let [gql-query (user-mentions-query {:n 25
                                        :after after
                                        :user (get-in cofx [:db :user])})
        query (merge (g/build-xhrio-query
                      (-> cofx :db :token)
                      {:uri "https://api.github.com/graphql"
                       :query (str "query mentions " gql-query)})
                     {:on-success [:user-mentions/result]
                      :on-failure [:user-mentions/result]}
                     )]
    {:db (-> (:db cofx)
             (assoc-in [:loading 'mentions] :loading))
     :http-xhrio query})))

(rf/reg-event-fx
 :user-mentions/result
 (defn-traced process-mentions
   [{:keys [db]} [_ result]]
   (reset! *result result)
   (if (contains? result :data)
     (let [nodes (get-in result [:data :search :nodes])
           prs (mapv format-pr nodes)
           end-cursor (read-next-cursor (get-in result [:data :search]))]
       (merge
        {:db (-> (cond-> db
                   cfg/debug? (assoc :debug-success result))
                 (assoc :mentions prs)
                 (assoc-in [:loading 'mentions] :ok))}
        (when end-cursor
          {:fx [[:dispatch [:user-mentions/fetch {:after end-cursor}]]]})))
     {:db (-> db
              (assoc-in [:loading 'mentions] :ko)
              (assoc :debug-error result))})))

;; TODO build a better build-xhrio-query
;;  - easier switch between standard urls
;;  - easier callback settings on-sucess/on-failure are always the same
;; TODO build a better tool to wrap generated queries than having to write "query name ..."
(rf/reg-event-fx
 :merged-prs/fetch
 (fn-traced
  [cofx [_ nameWithOwner]]
  (let [loading-key [:merged nameWithOwner]]
    (when-not (= :loading (get-in cofx [:db :loading loading-key]))
      (let [query (merge (g/build-xhrio-query
                          (-> cofx :db :token)
                          {:uri "https://api.github.com/graphql"
                           :query (str "query mergedprs"
                                       (merged-prs-query {:n 100
                                                          :repository nameWithOwner}))})
                         {:on-success [:merged-prs/result nameWithOwner]
                          :on-failure [:merged-prs/result nameWithOwner]})]
        {:db (assoc-in (:db cofx) [:loading loading-key] :loading)
         :http-xhrio query})))))

;; TODO build a standard way of fetching graphql results
;;  - loading key has similar logic
;;  - storing success/error
(rf/reg-event-db
 :merged-prs/result
 (defn-traced handle-merged-prs-result
   [db [_ nameWithOwner result]]
   (reset! *result result)
   (let [loading-key [:merged nameWithOwner]]
     (if (contains? result :data)
       (let [nodes (get-in result [:data :search :nodes])
             prs (map format-pr nodes)
             prs-of-interest (filter #(interesting-pr? db %) prs)]
         (-> (cond-> db
               cfg/debug? (assoc :debug-success result))
             (update :merged into prs-of-interest)
             (assoc-in [:loading loading-key] :ok)))
       (-> db
           (assoc-in [:loading loading-key] :ko)
           (assoc :debug-error result))))))

(rf/reg-event-fx
 :user-reviews/fetch
 (fn-traced
  [cofx [_ {:keys [after]}]]
  (let [gql-query (review-requests-query {:n 25
                                          :after after
                                          :user (get-in cofx [:db :user])})
        query (merge (g/build-xhrio-query
                      (-> cofx :db :token)
                      {:uri "https://api.github.com/graphql"
                       :query (str "query mentions " gql-query)})
                     {:on-success [:user-reviews/result]
                      :on-failure [:user-reviews/result]})]
    {:db (-> (:db cofx)
             (assoc-in [:loading 'reviews] :loading))
     :http-xhrio query})))

(rf/reg-event-fx
 :user-reviews/result
 (defn-traced process-reviews
   [{:keys [db]} [_ result]]
   (reset! *result result)
   (if (contains? result :data)
     (let [nodes (get-in result [:data :search :nodes])
           pr-idents (mapv format-pr-idents nodes)
           end-cursor (read-next-cursor (get-in result [:data :search]))]
       (merge
        {:db (-> (cond-> db
                   cfg/debug? (assoc :debug-success result))
                 (update :reviews set/union pr-idents)
                 (assoc-in [:loading 'reviews] :ok))}
        (when end-cursor
          {:fx [[:dispatch [:user-reviews/fetch {:after end-cursor}]]]})))
     {:db (-> db
              (assoc-in [:loading 'mentions] :ko)
              (assoc :debug-error result))})))

(comment
  (handle-pr-result {:user "Kineolyan"
                     :config {:mates ["fabiencelier" "OPeyrusse"]}}
                    [nil "fabiencelier" "universions" @*result]))

(defn parse-project
  [value]
  (zipmap
   [:owner :repository]
   (str/split value #"/")))

(comment
  (map parse-project (-> (get-db) :config :projects)))

(rf/reg-event-fx
 :load-prs
 (fn-traced
  [{:keys [db]} [_ automatic?]]
  (let [project-list (get-in db [:config :projects])
        projects (map parse-project project-list)
        project-fxs (mapv #(vector :dispatch [:project-prs/fetch %]) projects)
        mentions-fx [:dispatch [:user-mentions/fetch]]
        reviews-fx [:dispatch [:user-reviews/fetch]]
        merged-fxs (mapv #(vector :dispatch [:merged-prs/fetch %]) project-list)
        reload-fx {:action :start
                   :id ::load-prs
                   :frequency (get-in db [:config :refresh-rate] db/default-refresh-frequency)
                   :event [:load-prs :automatic]}]
    (merge
     {:db (assoc db
                 :prs []
                 :merged []
                 :mentions []
                 :reviews #{}
                 :loading {})
      :fx (concat project-fxs
                  [mentions-fx reviews-fx]
                  merged-fxs)}
     (when-not automatic?
       {:interval reload-fx})))))

;;; Initialization

(rf/reg-event-fx
 ::initialize-db
 [(rf/inject-cofx :local-store-token db/storage-key)
  (rf/inject-cofx ::store/read-storage db/config-key)]
 (fn-traced
  [{token :local-store-token storage ::store/storage} _]
  (let [new-db (merge db/default-db
                      {:route (if (str/blank? token) :settings :dashboard)
                       :token token
                       :config (get storage db/config-key {})})]
    {:db new-db
     :fx [(when token [:dispatch [:github-connection/check]])]})))

;;; Manual triggers

(comment
  (rf/dispatch [:user-mentions/fetch])
  (rf/dispatch [:load-prs])

  (rf/dispatch [:github-connection/check])

  (rf/dispatch [:project-prs/fetch {:owner "Kineolyan" :repository "project-tz-io"}])
  (rf/dispatch [:project-prs/fetch {:owner "activeviam" :repository "activepivot"}])
  (rf/dispatch [:user-mentions/fetch])
  (rf/dispatch [:merged-prs/fetch "Kineolyan/project-tz-io"]))
