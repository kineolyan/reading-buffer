(ns shared.fauna-date
  (:require [cljs-time.coerce :as time-coerce]
            [cljs-time.format :as time-format]
            ["faunadb" :as f]))

(defn fdb->str
  [value]
  (condp = (type value)
    f/values.FaunaDate (.-value value)
    f/values.FaunaTime (.-value value)
    js/String value))

(comment
  (fdb->str "203-1-1")
  (fdb->str (f/values.FaunaDate. "2022-10-05")))

(def fdb-date-format (time-format/formatters :date))
(def fdb-time-format (time-format/formatters :date-time-no-ms))

(def supported-time-formats
  [fdb-time-format
   (time-format/formatters :date-time)])

(defn ->fdb-date
  "Converts a Javascript timestamp into a FaunaDB Date."
  [timestamp]
  (some->> timestamp
           (time-coerce/from-long)
           (time-format/unparse fdb-date-format)))

(defn ->fdb-time
  "Converts a Javascript timestamp into a FaunaDB Time."
  [timestamp]
  (some->> timestamp
           (time-coerce/from-long)
           (time-format/unparse fdb-time-format)))

(defn date->timestamp
  "Converts a FaunaDB Date into a Javascript timestamp."
  [date-str]
  (some->> date-str
           (fdb->str)
           (time-format/parse fdb-date-format)
           (time-coerce/to-long)))

(defn datetime->timestamp
  "Converts a FaunaDB Time into a Javascript timestamp."
  ([date-str]
   (when date-str
     (->> supported-time-formats
          (keep #(datetime->timestamp date-str %))
          (first))))
  ([date-str format]
   (try
     (some->> date-str
              (fdb->str)
              (time-format/parse format)
              (time-coerce/to-long))
     (catch js/Error _ nil))))

(comment
  (date->timestamp "2021-08-20")
  (date->timestamp nil)
  (datetime->timestamp "2021-08-20T12:58:25Z")
  (datetime->timestamp nil)
  (->fdb-time (js/Date.now))
  (->fdb-time nil)
  (->fdb-date (js/Date.now))
  (->fdb-date nil))
