(ns shared.interval-fx
  "
  Usage:
  Event to trigger a schedule
  ```clojure
  {:interval {:action    :start
              :id        :panel-1-query     ;; my id for this (so I can cancel later)
              :frequency {:min 3}           ;; how many ms between dispatches
              :event     [:panel-query 1]}} ;; what to dispatch
  ```
  And to later cancel the regular dispatch, an event handler would return this:
  ```clojure
  {:interval  {:action    :cancel
               :id        :panel-1-query}}   ;; the id provided to :start
  ```
  "
  (:require [re-frame.core :as rf]
            [day8.re-frame.tracing :refer-macros [defn-traced]]))

; From https://github.com/day8/re-frame/blob/3c6712230a3dde2f8afa1babe07eb2be2725e5ad/docs/FAQs/PollADatabaseEvery60.md

(def units
  (as-> {:ms 1} m
    (assoc m :sec (* 1000 (get m :ms)))
    (assoc m :min (* 60 (get m :sec)))
    (assoc m :hour (* 60 (get m :min)))))

(defn interval->millis
  ([interval]
   (->> interval
        (map #(apply interval->millis %))
        (reduce +)))
  ([unit n]
   {:pre [(contains? units unit)]}
   (* n (get units unit))))

(comment
  (interval->millis nil))

(defonce live-intervals (atom {}))

(declare interval-handler)

(defn start-interval!
  [{:keys [id frequency event] :as payload}]
  (if (and (some? frequency)
           (pos? (interval->millis frequency)))
    (let [f (fn future-action []
                  ; Dispatch the event
              (rf/dispatch event)
                  ; Reschedule the action
              (interval-handler payload))
          interval-id (js/setTimeout f (interval->millis frequency))]
      (when-let [existing-id (get @live-intervals id)]
        (js/clearTimeout existing-id))
      (swap! live-intervals assoc id interval-id))
    (js/console.error "Ignoring event for" id ". No frequency provided")))

(comment
  ; Checking that nothing is triggered for this
  (start-interval! {:id :something
                    :event [::not-an-event 1]}))

(defn stop-interval!
  [{:keys [id]}]
  (when-let [timeout-id (get @live-intervals id)]
    (js/clearTimeout timeout-id)
    (swap! live-intervals dissoc id)))

(defn clean-intervals!
  []
  ; trigger an event to stop all events
  (doseq [id (keys @live-intervals)]
    (interval-handler {:action :end  :id id})))

(defn-traced interval-handler
  [{:keys [action id] :as payload}]
  (js/console.log "triggering " id "on" action)
  (case action
    :clean (clean-intervals!)
    :start (start-interval! payload)
    :end (stop-interval! payload))
  nil)

(defn reset-fx!
  "Resets all previously registered events, avoiding abandonned schedules."
  []
  (interval-handler {:action :clean}))

; when this code is reloaded `:clean` existing intervals
(reset-fx!)

(rf/reg-fx
 :interval
 interval-handler)
