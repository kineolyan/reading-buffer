(ns shared.graphql
  (:require [ajax.core :as ajax]))

(defn build-xhrio-query
  [token {:keys [uri query variables]}]
  {:method :post
   :uri uri
   :params {:query query
            :variables variables}
   :format (ajax/json-request-format)
   :response-format (ajax/json-response-format {:keywords? true})
   :with-credentials false
   :headers {"Authorization" (str "Bearer " token)}})
