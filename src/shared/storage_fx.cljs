(ns shared.storage-fx
  (:require [clojure.edn :as edn]
            [clojure.core.match :refer-macros [match]]
            [re-frame.core :as rf]))

(defn clj->local-store
  "Saves a value to the Local Storage with the key `k`."
  [k v]
  (.setItem js/localStorage k (pr-str v)))

(defn local-store->clj
  "Reads the value from the Local Storage at key `k`."
  [k]
  (some->
   (.getItem js/localStorage k)
   (edn/read-string)))

(rf/reg-fx
 ::storage
 (fn [payload]
   (match [payload]
     [[:write k v]] (clj->local-store k v))))

(rf/reg-cofx
 ::read-storage
 (fn [cofx k]
   (update-in cofx [::storage] assoc k (local-store->clj k))))

(comment
  ; testing with fake handlers

  (rf/reg-event-fx
   ::test-write
   (fn
     [cofx [_ k v]]
     {::storage [:write k v]}))

  (rf/reg-event-fx
   ::test-read
   [(rf/inject-cofx ::read-storage "random-key-for-test")
    (rf/inject-cofx ::read-storage "none")]
   (fn [cofx _]
     (tap> [:result (get-in cofx [::storage])])))

  (rf/dispatch [::test-write "random-key-for-test" {:C [:a 2 "u"]}])
  (rf/dispatch [::test-read]))
