(ns shared.fauna-fx
  (:require [re-frame.core :as rf]
            [promesa.core :as p]
            [ajax.core :as ajax]
            ["faunadb" :as f]
            [shared.fauna-query :as fq]))

(defn token->local-store
  "Saves the Fauna token to the Local Storage with the key `k`."
  [k token]
  (.setItem js/localStorage k token))

(defn local-store->token
  "Reads the Fauna token from the Local Storage at key `k`."
  [k]
  (.getItem js/localStorage k))

(rf/reg-cofx
 :local-store-token
 (fn [cofx k]
   (assoc cofx :local-store-token (local-store->token k))))

(defn get-token-from-url!
  []
  (-> js/window
      .-location
      .-search
      (subs 1)
      js/URLSearchParams.
      (.get "token")))

(rf/reg-cofx
 :url-token
 (fn [cofx _]
   (assoc cofx :url-token (get-token-from-url!))))

;;; Query helpers

(defn fauna-query
  [cofx graph-req]
  (let [token (get-in cofx [:db :token])]
    {:fauna [(assoc graph-req :token token)]}))


;;; new fx with fauna ql

(def client (atom nil))

(defn -create-client
  [token]
  (let [client (f/Client. #js{:secret token})]
    (js/console.log "Allocating a new client")
    {:client client
     :token token}))

(defn -get-client
  [state token]
  (if (= (:token state) token)
    state
    (-create-client token)))

(defn get-client!
  [token]
  (let [result (swap! client -get-client token)]
    (result :client)))

(defn read-item
  [item]
  (assoc (js->clj (aget item "data") :keywordize-keys true)
         :_id (.-id (aget item "ref"))
         :_ts (aget item "ts")))

(defn read-page
  [page]
  (def page* page)
  (let [before (aget page "before")
        after (aget page "after")
        data  (aget page "data")]
    {:before before
     :after after
     :data (map read-item data)}))

(defn -query-and-dispatch!
  [{:keys [query token on-success on-failure]}]
  (let [c (get-client! token)]
    (-> (.query c query)
        (.then #(rf/dispatch (conj (vec on-success) %))
               #(rf/dispatch (conj (vec on-failure) %))))))

(defn -create-page-query
  [{:keys [collection page-size after]}]
  (let [options (merge {:size page-size} (when after {:after after}))]
    (fq/Map
     (fq/Paginate
      (fq/Documents (fq/Collection collection))
      (clj->js options))
     (fq/Lambda "e" (fq/Get (fq/Var "e"))))))

(defn -process-page-result
  [payload]
  (let [{:keys [data after]} (read-page payload)
        marker (if after {:page :partial} {:page :last})]
    {:result (merge marker {:items data})
     :next-page after}))

(defn -read-and-dispatch!
  [{:keys [token on-success on-failure] read-query :read :as args}]
  (let [query (-create-page-query read-query)
        client (get-client! token)]
    (p/handle
     (p/let [result (.query client query)]
       (-process-page-result result))
     (fn [{:keys [result next-page]} error]
       (let [message (if error
                       (conj (vec on-failure) error)
                       (conj (vec on-success) result))]
         (rf/dispatch message))
       (when next-page
         (-read-and-dispatch!
          (assoc-in args [:read :after] next-page)))))))

(defn -update-and-dispatch!
  [{:keys [token on-success on-failure] update-query :update}]
  (let [{{:keys [collection id]} :target updated-values :values} update-query
        query (fq/Update
               (fq/Ref (fq/Collection collection) id)
               (clj->js {:data updated-values}))
        client (get-client! token)]
    (p/handle
     (p/let [result (.query client query)]
       (read-item result))
     (fn [result error]
       (let [message (if error
                       (conj (vec on-failure) error)
                       (conj (vec on-success) result))]
         (rf/dispatch message))))))

(defn -create-and-dispatch!
  [{:keys [token on-success on-failure] create-query :create :as query}]
  (let [{:keys [collection datum]} create-query
        query (fq/Create
               (fq/Collection collection)
               (clj->js {:data datum}))
        client (get-client! token)]
    (p/handle
     (p/let [result (.query client query)]
       (read-item result))
     (fn [result error]
       (let [message (if error
                       (conj (vec on-failure) error)
                       (conj (vec on-success) result))]
         (rf/dispatch message))))))

(rf/reg-fx
 :fauna
 (fn [request]
   (doseq [query request]
     (cond
       (:read query) (-read-and-dispatch! query)
       (:update query) (-update-and-dispatch! query)
       (:create query) (-create-and-dispatch! query)
       (:query query) (-query-and-dispatch! query)))))
